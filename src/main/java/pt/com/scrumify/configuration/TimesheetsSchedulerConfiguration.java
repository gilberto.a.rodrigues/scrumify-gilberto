package pt.com.scrumify.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.services.TimesheetsSchedulerService;

@Configuration
@EnableScheduling
@ConditionalOnProperty(value = ConstantsHelper.SCHEDULER_TIMESHEETS_ENABLED, havingValue = "true")
public class TimesheetsSchedulerConfiguration {
   private static final Logger logger = LoggerFactory.getLogger(TimesheetsSchedulerConfiguration.class);

   @Autowired
   TimesheetsSchedulerService scheduler;

   @Scheduled(fixedDelayString = ConstantsHelper.SCHEDULER_TIMESHEETS_FIXED_DELAY, initialDelayString = ConstantsHelper.SCHEDULER_TIMESHEETS_INITIAL_DELAY)
   public void timesheetsSchedulerWorker() {
      logger.info("#timesheetscheduler start at {}.", DatesHelper.now());

      scheduler.run();

      logger.info("#timesheetscheduler ends at {}.", DatesHelper.now());
   }
}
package pt.com.scrumify.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;
import pt.com.scrumify.helpers.ConstantsHelper;

@Configuration
public class JavaScriptThymeleafConfiguration implements ApplicationContextAware, WebMvcConfigurer {
   private ApplicationContext applicationContext;

   @Override
   public void setApplicationContext(ApplicationContext applicationContext) {
      this.applicationContext = applicationContext;
   }

   @Bean
   public ViewResolver javascriptViewResolver() {
      ThymeleafViewResolver resolver = new ThymeleafViewResolver();
      resolver.setTemplateEngine(templateEngine(javascriptTemplateResolver()));
      resolver.setContentType(ConstantsHelper.MIME_TYPE_JAVASCRIPT);
      resolver.setCharacterEncoding(ConstantsHelper.ENCODING_UTF8);
      
      return resolver;
   }

   private ITemplateResolver javascriptTemplateResolver() {
      SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
      resolver.setApplicationContext(applicationContext);
      resolver.setSuffix(".js");
      resolver.setTemplateMode(TemplateMode.JAVASCRIPT);
      
      return resolver;
   }

   private ISpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
      SpringTemplateEngine engine = new SpringTemplateEngine();
      engine.setTemplateResolver(templateResolver);
      
      return engine;
   }
}

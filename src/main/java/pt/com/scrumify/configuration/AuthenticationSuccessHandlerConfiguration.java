package pt.com.scrumify.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.entities.ScrumifyUser;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationSuccessHandlerConfiguration implements AuthenticationSuccessHandler {
   private final Logger logger = LoggerFactory.getLogger(this.getClass());
   private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

   @Override
   public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
      Resource resource = ((ScrumifyUser) authentication.getPrincipal()).getResource();
      
      /*
       * Log authentication success
       */
      logger.info("Authentication succeeded : Login success for user `{}`.", resource.getUsername());

      Object redirectURL = request.getSession().getAttribute(ConstantsHelper.REDIRECT_URL_SESSION_ATTRIBUTE);
      String url = ConstantsHelper.MAPPING_ROOT;
      
      if (redirectURL != null) {
        url = redirectURL.toString();
      }

      /*
       * Redirect to change pin page if last password change is null, otherwise go to default page (index)
       */
      if (resource.getLastPasswordChange() == null) {
         logger.info("Authentication succeeded : Resource `{}` must change pin, redirecting to change pin page.", resource.getUsername());
         
         url =  ConstantsHelper.MAPPING_RESOURCES_CHANGEPIN;
      }
      
      redirectStrategy.sendRedirect(request, response, url);
   }
}
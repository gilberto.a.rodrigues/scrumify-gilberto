package pt.com.scrumify.services;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public class HibernateSearchService {
   
   @Autowired
   private final EntityManager entityManager;

   
   @Autowired
   public HibernateSearchService(EntityManager entityManager) {
       super();
       this.entityManager = entityManager;
   }
   
   public void initializeHibernateSearch() throws InterruptedException {
      FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
      fullTextEntityManager.createIndexer().startAndWait();
   }
  
}

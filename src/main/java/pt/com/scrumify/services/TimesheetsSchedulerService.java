package pt.com.scrumify.services;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.database.services.*;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.TimesheetReviewHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TimesheetsSchedulerService {
   private static final Logger logger = LoggerFactory.getLogger(TimesheetsSchedulerService.class);
   
   @Autowired
   AbsenceService absences;
   @Autowired
   CompensationDayService compensationDays;
   @Autowired
   HolidayService holidays;
   @Autowired
   ResourceService resources;
   @Autowired
   TimeScheduleService timesSchedule;
   @Autowired
   TimesheetService timesheets;
   @Autowired
   VacationService vacations;
   @Autowired
   WorkItemWorkLogService workLogs;
   @Autowired
   YearService years;
   @Autowired
   private TimesheetReviewHelper timesheetReviewHelper;
   @Autowired
   SecurityService securityService;
   
   public void run() {
      List<CompensationDay> cds = compensationDays.getAll();
      List<Holiday> hs = holidays.getAll();
      
      for (Resource resource : resources.getByApproved(true)) {
         if (!resource.getUsername().equals("admin")) {
            List<Absence> abs = absences.listByResource(resource);
            List<Vacation> vs = vacations.listByResource(resource);
            List<WorkItemWorkLog> wls = workLogs.getByResource(resource);
            
            /*
             * Set security context for current resource
             */
            logger.info("Resource: {}", resource.getName());
            securityService.setSecurityContext(resource);
         
            processResources(resource, abs, cds, hs, vs, wls);
         }
      }
   }

   private boolean isValid(Resource resource, Date date) {
      if (resource.getStartingDay().compareTo(date) <= 0 && (resource.getEndingDay() == null || resource.getEndingDay().compareTo(date) >= 0)) {
         return true;
      }
      
      return false;
   }

   private boolean isValid(Resource resource, Integer year, Integer month, Integer fortnight) {
      DateTime q1s = new DateTime(year, month, 1, 0, 0);
      DateTime q1f = new DateTime(year, month, 15, 0, 0);
      DateTime q2s = new DateTime(year, month, 16, 0, 0);
      DateTime q2f = month != 12 ? 
            new DateTime(year, month + 1, 1, 0, 0).minusDays(1) : 
            new DateTime(year + 1, 1, 1, 0, 0).minusDays(1);
      
      if (fortnight == 1 && resource.getStartingDay().compareTo(q1f.toDate()) <= 0 && (resource.getEndingDay() == null || resource.getEndingDay().compareTo(q1s.toDate()) >= 0)) {
         return true;
      }

      if (fortnight == 2 && resource.getStartingDay().compareTo(q2f.toDate()) <= 0 && (resource.getEndingDay() == null || resource.getEndingDay().compareTo(q2s.toDate()) >= 0)) {
         return true;
      }
      
      return false;
   }
   
   private void processResources(Resource resource, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      for (Year year : years.getAllActive()) {
         processYears(resource, year, abs, cds, hs, vs, wls);
      }
   }
   
   private void processYears(Resource resource, Year year, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      List<Integer> months = new ArrayList<>();
      months.add(1);
      months.add(2);
      months.add(3);
      months.add(4);
      months.add(5);
      months.add(6);
      months.add(7);
      months.add(8);
      months.add(9);
      months.add(10);
      months.add(11);
      months.add(12);
      
      for (Integer month : months) {
         processMonths(resource, year, month, abs, cds, hs, vs, wls);
      }
   }
   
   private void processMonths(Resource resource, Year year, Integer month, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      List<Integer> fortnights = new ArrayList<>();
      fortnights.add(1);
      fortnights.add(2);
      
      for (Integer fortnight : fortnights) {
         processFortnights(resource, year, month, fortnight, abs, cds, hs, vs, wls);
      }
   }
   
   private void processFortnights(Resource resource, Year year, Integer month, Integer fortnight, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      Timesheet t = timesheets.getSheetByFortnight(resource, year.getId(), month, fortnight);
      if (isValid(resource, year.getId(), month, fortnight)) {
         if (t == null) {
            createTimesheet(resource, year, month, fortnight, abs, cds, hs, vs, wls);
         }
         else {
            updateTimesheet(t, false, resource, year, month, fortnight, abs, cds, hs, vs, wls);
         }
      }
      else if (t != null && t.getStatus().getId().equals(TimesheetStatus.NOT_SUBMITTED)) {
         logger.info("Invalid timesheet detected, will be removed! Resource: {}, date: {}-{} {}Q.", resource.getName(), year.getId().toString(), month.toString(), fortnight.toString());
         
         timesheets.delete(t.getId());
      }
      else if (t != null && t.getStatus() != null) {
         logger.info("Invalid timesheet detected! Resource: {}, date: {}-{} {}Q.", resource.getName(), year.getId().toString(), month.toString(), fortnight.toString());
      }
   }
   
   private void createTimesheet(Resource resource, Year year, Integer month, Integer fortnight, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      DateFormat formatter = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      DateTime qs = fortnight == 1 ? new DateTime(year.getId(), month, 1, 0, 0) : new DateTime(year.getId(), month, 16, 0, 0);
      DateTime qf = fortnight == 1 ? 
            new DateTime(year.getId(), month, 15, 0, 0) : 
            (month != 12 ? 
                  new DateTime(year.getId(), month + 1, 1, 0, 0).minusDays(1) : 
                  new DateTime(year.getId() + 1, 1, 1, 0, 0).minusDays(1));
      
      Timesheet t = new Timesheet();
      t.setResource(resource);
      t.setStartingDay(new Time(formatter.format(qs.toDate())));
      t.setEndingDay(new Time(formatter.format(qf.toDate())));
      t.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
      t.setSubmitted(false);
      t.setApproved(false);
      t.setReviewed(false);
      
      updateTimesheet(t, true, resource, year, month, fortnight, abs, cds, hs, vs, wls);
   }
   
   private void updateTimesheet(Timesheet t, boolean newTimesheet, Resource resource, Year year, Integer month, Integer fortnight, List<Absence> abs, List<CompensationDay> cds, List<Holiday> hs, List<Vacation> vs, List<WorkItemWorkLog> wls) {
      List<TimeSchedule> timeSchedules = timesSchedule.getByYearMonthAndFortnight(year, month, fortnight, resource.getSchedule());
      boolean updateTimesheet = false;
      
      if (newTimesheet)
         logger.info("New timesheet will be created for '{}' on {}-{} {}Q.", resource.getName(), year.getId().toString(), month.toString(), fortnight.toString());
      
      for (TimeSchedule ts : timeSchedules) {
         TimesheetDay td = new TimesheetDay();
         List<TimesheetDay> ftds = t.getDays().stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (ftds.isEmpty()) {
            /*
             * The timesheetday doesn't exists, let's fill base fields (day, timesheet, compensation day, holiday and weekend)
             */            
            td.setDay(ts.getTime());
            td.setTimesheet(t);
            td.setCompensationDay(ts.getTime().isCompensationDay());
            td.setHoliday(ts.getTime().isHoliday());
            td.setWeekend(ts.getTime().isWeekend());
            
            updateTimesheet = true;
         }
         else {
            /*
             * The timesheetday already exists, get object to update
             */
            td = ftds.get(0);
         }
         
         /*
          * Hours
          */
         if (isValid(resource, ts.getTime().getDate())) {
            if (!newTimesheet && td.getHours() != ts.getHours()) {
               logger.info("Timesheet (hours) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHours(ts.getHours());
         }
         else {
            if (!newTimesheet && (td.getHours() == null || td.getHours() != 0)) {
               logger.info("Timesheet (hours) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHours(0);
         }

         /*
          * Absences
          */
         List<Absence> fabs = abs.stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (!fabs.isEmpty()) {
            if (!newTimesheet && td.getHoursAbsence() != fabs.stream().mapToInt(Absence::getHours).sum()) {
               logger.info("Timesheet (absence) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHoursAbsence(fabs.stream().mapToInt(Absence::getHours).sum());
         }

         /*
          * Compensation Days
          */
         List<CompensationDay> fcds = cds.stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (!fcds.isEmpty()) {
            if (!newTimesheet && td.getHoursCompensationDay() != ts.getHours()) {
               logger.info("Timesheet (compensation day) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHoursCompensationDay(ts.getHours());
         }

         /*
          * Holidays
          */
         List<Holiday> fhs = hs.stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (!fhs.isEmpty()) {
            if (!newTimesheet && td.getHoursHoliday() != ts.getHours()) {
               logger.info("Timesheet (holiday) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHoursHoliday(ts.getHours());
         }
         
         /*
          * Vacations
          */
         List<Vacation> fvs = vs.stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (!fvs.isEmpty()) {
            if (!newTimesheet && td.getHoursVacation() != ts.getHours()) {
               logger.info("Timesheet (vacation) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setHoursVacation(ts.getHours());
         }
         
         /*
          * WorkItem Work Log
          */
         List<WorkItemWorkLog> fwls = wls.stream().filter(x -> x.getDay().getId().equals(ts.getTime().getId())).collect(Collectors.toList());
         if (!fwls.isEmpty()) {
            if (!newTimesheet && td.getTimeSpent() != fwls.stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum()) {
               logger.info("Timesheet (time spent) updated for resource '{}' on {}.", resource.getName(), ts.getTime().getDate());
               updateTimesheet = true;
            }
            
            td.setTimeSpent(fwls.stream().mapToInt(WorkItemWorkLog::getTimeSpent).sum());
         }
         
         if (ftds.isEmpty()) {
            /*
             * Only add new timesheetday if it doesn't exists
             */
            t.getDays().add(td);
         }
      }
      
      /*
       * Save data only if timesheet is not submitted
       */
      if (t.getStatus().getId().equals(TimesheetStatus.NOT_SUBMITTED)) {         
         /*
          * Save if is a new timesheet or timesheet data changed
          */
         if (newTimesheet || updateTimesheet) {
            timesheets.save(t);
         }

         /*
          * Timesheet review (set compensation days and holidays hours)
          */
         /*
          * TODO: convert to constant
          */
         Integer hours = t.getDays().stream().mapToInt(TimesheetDay::getHoursCompensationDay).sum(); 
         if (hours > 0) {
            timesheetReviewHelper.set(TypeOfWork.OTHER, year, month, fortnight, null, "99-55", "Compensation Day", hours);
         }
         hours = t.getDays().stream().mapToInt(TimesheetDay::getHoursHoliday).sum(); 
         if (hours > 0) {
            timesheetReviewHelper.set(TypeOfWork.OTHER, year, month, fortnight, null, "99-55", "Holiday", hours);
         }
      }         
   }
}
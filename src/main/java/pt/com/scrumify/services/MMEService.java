package pt.com.scrumify.services;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Financial;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.services.FinancialService;
import pt.com.scrumify.entities.PairIntegerDate;
import pt.com.scrumify.entities.PairIntegerString;
import pt.com.scrumify.entities.UploadInfo;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.MMEHelper;
import pt.com.scrumify.helpers.YearsHelper;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MMEService {
   private static final String ACTUAL = "Actual";
   private static final String COST_RATE = "Cost Rate";
   private static final String CTD = "CTD";
   private static final String FORECAST = "Forecast";
   private static final String HOURS = "Hours";
   private static final String PAYROLL = "Payroll";
   private static final String REMAINING_BUDGET_AMT = "RemainingBudgetAmt";
   private static final String TBDR = "TBDR";
   private static final String USED_FOR_RISK_AMT = "UsedForRiskAmt";
   private static final String WRITE_UP_CI_AMT = "WriteUpCIAmt";

   private static final String CATEGORY_ADJUSTED_PERCENTAGE_COMPLETE = "Adjusted % Complete";
   private static final String CATEGORY_ADJUSTED_CCI = "Adjusted CCI";
   private static final String CATEGORY_ADJUSTED_CCI_PERCENTAGE = "Adjusted CCI %";
   private static final String CATEGORY_ADJUSTED_CUMULATIVE_POC_COSTS = "Adjusted Cumulative PoC Costs";
   private static final String CATEGORY_ADJUSTED_TOTAL_CONTRACT_COSTS = "Adjusted Total Contract Costs";
   private static final String CATEGORY_ADJUSTED_TOTAL_NON_POC_COSTS = "Adjusted Total Non-PoC Costs";
   private static final String CATEGORY_ADJUSTED_TOTAL_POC_COSTS = "Adjusted Total PoC Costs";
   private static final String CATEGORY_ADJUSTED_TOTAL_SERVICE_REVENUE = "Adjusted Total Service Revenue";
   private static final String CATEGORY_CTD_POC_SERVICE_REVENUE_ADJUSTMENT = "CTD PoC Service Revenue Adjustment";
   private static final String CATEGORY_PREFIX_EAC = "EAC - ";
   private static final String CATEGORY_SUBCONTRACTOR_LABOR = "Subcontractor Labor";
   private static final String CATEGORY_TOTAL_CONTRACT_VALUE_SERVICES = "Total Contract Value - Services";
   private static final String CATEGORY_WORKING_CAPITAL_OTHER = "Working Capital Other";

   private static final String FINANCIALS_TYPE_POC_DETAILS = "PoC Details";
   private static final String FINANCIALS_TYPE_RESOURCE_TREND = "Resource Trend";
   private static final String FINANCIALS_TYPE_SOLUTION_CONTINGENCY = "Solution Contingency";

   private static final int POC_DETAILS_COLUMN_ACTUAL_FORECAST = 11;
   private static final int POC_DETAILS_ROW_MONTHS = 11;
   private static final int RESOURCES_TREND_COLUMN_ACTUAL_FORECAST = 16;
   private static final int RESOURCES_TREND_COLUMN_CATEGORY = 14;
   private static final int RESOURCES_TREND_COLUMN_DATE = 17;
   private static final int RESOURCES_TREND_COLUMN_QUANTITY = 15;
   private static final int RESOURCES_TREND_COLUMN_NAME = 10;
   private static final int RESOURCES_TREND_COLUMN_ROLE_NAME = 9;
   private static final int SOLUTION_CONTINGENCY_COLUMN_ACTUAL_FORECAST = 10;
   private static final int SOLUTION_CONTINGENCY_COLUMN_AMOUNT = 4;
   private static final int SOLUTION_CONTINGENCY_COLUMN_MONTH = 3;
   private static final int SOLUTION_CONTINGENCY_COLUMN_REASON = 5;
   private static final int SOLUTION_CONTINGENCY_COLUMN_RISKID = 0;

   private static final String[] categoriesNames = {
      CATEGORY_ADJUSTED_PERCENTAGE_COMPLETE,
      CATEGORY_ADJUSTED_TOTAL_SERVICE_REVENUE,
      CATEGORY_CTD_POC_SERVICE_REVENUE_ADJUSTMENT,
      CATEGORY_TOTAL_CONTRACT_VALUE_SERVICES,
      CATEGORY_SUBCONTRACTOR_LABOR,
      CATEGORY_ADJUSTED_TOTAL_POC_COSTS,
      CATEGORY_ADJUSTED_CUMULATIVE_POC_COSTS,
      CATEGORY_WORKING_CAPITAL_OTHER,
      CATEGORY_ADJUSTED_TOTAL_NON_POC_COSTS,
      CATEGORY_ADJUSTED_TOTAL_CONTRACT_COSTS,
      CATEGORY_ADJUSTED_CCI,
      CATEGORY_ADJUSTED_CCI_PERCENTAGE
   };
   
   private static final Logger logger = LoggerFactory.getLogger(MMEService.class);

   @Autowired
   private FinancialService financialService;
   @Autowired
   private MMEHelper helper;
   @Autowired
   private YearsHelper yearHelper;

   public void processPoCDetails(Workbook workbook, Contract contract, UploadInfo info) {
      Sheet sheet = workbook.getSheet(ConstantsHelper.POC_DETAILS_SHEET_DATA);
      Iterator<Row> iterator = sheet.iterator();

      iterator.next(); // Skip header

      //
      // Get saved data
      //
      List<Financial> financials = financialService.getByContractAndType(contract, FINANCIALS_TYPE_POC_DETAILS);
      
      //
      // Get categories
      //
      List<PairIntegerString> categories = getPoCDetailsCategories(workbook);
      
      //
      // Get time limits
      //
      Date min = yearHelper.getMinYear();
      Date max = yearHelper.getMaxYear();
      List<PairIntegerDate> dates = getPoCDetailsDates(workbook, min, max);
      
      //
      // Get eac data
      //
      processPoCDetailColumnData(workbook, categories, financials, contract, 4 + dates.size(), formatDateAsString(max), true, info);
      
      //
      // Get monthly data
      //
      for (PairIntegerDate date : dates) {
         //
         // Jump to next line if date are outside the years limits
         //
         if (date.getValue().compareTo(min) < 0 || date.getValue().compareTo(max) > 0)
            continue;         

         processPoCDetailColumnData(workbook, categories, financials, contract, date.getIndex(), formatDateAsString(date.getValue()), false, info);
      }
   }

   private void processPoCDetailColumnData(Workbook workbook, List<PairIntegerString> categories, List<Financial> financials, Contract contract, int column, String time, boolean eac, UploadInfo info) {
      for (PairIntegerString category : categories) {
         int row = category.getIndex();
         String categoryName = eac ? CATEGORY_PREFIX_EAC + category.getValue() : category.getValue();
         boolean actual = helper.parseCellAsString(workbook, ConstantsHelper.POC_DETAILS_SHEET_DATA, POC_DETAILS_COLUMN_ACTUAL_FORECAST, column, ConstantsHelper.EXCEL_STRING_FORMAT).contains(ACTUAL);
         BigDecimal value = helper.parseCellAsBigDecimal(workbook, ConstantsHelper.POC_DETAILS_SHEET_DATA, row, column);
                  
         save(financials, contract, time, FINANCIALS_TYPE_POC_DETAILS, categoryName, null, actual, false, value, eac, info);
      }
   }
   
   public void processResourceTrend(Workbook workbook, Contract contract, UploadInfo info) {
      Sheet sheet = workbook.getSheet(ConstantsHelper.RESOURCES_TREND_SHEET2);
      Iterator<Row> iterator = sheet.iterator();

      iterator.next(); // Skip header

      //
      // Get saved data
      //
      List<Financial> financials = financialService.getByContractAndType(contract, FINANCIALS_TYPE_RESOURCE_TREND);
      
      //
      // Get time limits
      //
      Date min = yearHelper.getMinYear();
      Date max = yearHelper.getMaxYear();
      
      //
      // Iterate sheet
      //
      while (iterator.hasNext()) {
         Row row = iterator.next();

         Date date = helper.parseCellAsDate(row, RESOURCES_TREND_COLUMN_DATE, ConstantsHelper.DEFAULT_FORMAT_DATE_EXCEL);
         
         //
         // Stop while when empty cell is detected
         //
         if (date == null)
            break;
         
         //
         // Jump to next line if date are outside the years limits
         //
         if (date.compareTo(min) >= 0 && date.compareTo(max) <= 0) {
            String time = formatDateAsString(date);
            String category = helper.parseCellAsString(row, RESOURCES_TREND_COLUMN_CATEGORY, ConstantsHelper.EXCEL_STRING_FORMAT);
            String enterpriseid = helper.parseCellAsString(row, RESOURCES_TREND_COLUMN_NAME, ConstantsHelper.EXCEL_STRING_FORMAT).toLowerCase();
            boolean actual = helper.parseCellAsString(row, RESOURCES_TREND_COLUMN_ACTUAL_FORECAST, ConstantsHelper.EXCEL_STRING_FORMAT).equalsIgnoreCase(ACTUAL);
            boolean tbdr = helper.parseCellAsString(row, RESOURCES_TREND_COLUMN_ROLE_NAME, ConstantsHelper.EXCEL_STRING_FORMAT).contains(TBDR);
            BigDecimal value = helper.parseCellAsBigDecimal(row, RESOURCES_TREND_COLUMN_QUANTITY);
   
            if (isResourceTrendCategory(category)) {
               save(financials, contract, time, FINANCIALS_TYPE_RESOURCE_TREND, category, enterpriseid, actual, tbdr, value, false, info);
            }
         }
      }
   }
   
   public void processSolutionContigency(Workbook workbook, Contract contract, UploadInfo info) {
      Sheet sheet = workbook.getSheet(ConstantsHelper.SOLUTION_CONTINGENCY_SHEET2);
      Iterator<Row> iterator = sheet.iterator();

      iterator.next(); // Skip header

      //
      // Get saved data
      //
      List<Financial> financials = financialService.getByContractAndType(contract, FINANCIALS_TYPE_SOLUTION_CONTINGENCY);
      
      //
      // Get time limits
      //
      Date min = yearHelper.getMinYear();
      Date max = yearHelper.getMaxYear();
      
      //
      // Iterate sheet
      //
      while (iterator.hasNext()) {
         Row row = iterator.next();

         Date date = helper.parseCellAsDate(row, SOLUTION_CONTINGENCY_COLUMN_MONTH, ConstantsHelper.DEFAULT_FORMAT_DATE_EXCEL);

         //
         // Stop while when empty cell is detected
         //
         if (date == null)
            break;
         
         //
         // Jump to next line if date are outside the years limits
         //
         if (date.compareTo(min) >= 0 && date.compareTo(max) <= 0) {
            String time = formatDateAsString(date);
            String category = helper.parseCellAsString(row, SOLUTION_CONTINGENCY_COLUMN_REASON, ConstantsHelper.EXCEL_STRING_FORMAT);
            String enterpriseid = helper.parseCellAsString(row, SOLUTION_CONTINGENCY_COLUMN_RISKID, ConstantsHelper.EXCEL_DECIMAL_10_0_FORMAT).toLowerCase();
            boolean actual = helper.parseCellAsString(row, SOLUTION_CONTINGENCY_COLUMN_ACTUAL_FORECAST, ConstantsHelper.EXCEL_STRING_FORMAT).equalsIgnoreCase(CTD);
            BigDecimal value = helper.parseCellAsBigDecimal(row, SOLUTION_CONTINGENCY_COLUMN_AMOUNT);
   
            if (isSolutionContingencyCategory(category)) {
               save(financials, contract, time, FINANCIALS_TYPE_SOLUTION_CONTINGENCY, category, enterpriseid, actual, false, value, false, info);
            }
         }
      }
   }
   
   private void save(List<Financial> financials, Contract contract, String time, String type, String category, String enterpriseId, boolean actual, boolean tbdr, BigDecimal value, boolean eac, UploadInfo info) {
      Financial entity = filter(financials, time, category, enterpriseId, eac);
      if (entity == null) {
         //
         // Save entity only if value != 0
         //
         if (value.compareTo(BigDecimal.ZERO) != 0) {
            entity = new Financial();
   
            entity.setContract(contract);
            entity.setTime(new Time(time));
            entity.setType(type);
            entity.setCategory(category);
            entity.setEnterpriseId(enterpriseId);
            entity.setActual(actual);
            entity.setTbdr(tbdr);
            entity.setValue(value);
   
            financialService.save(entity);
            info.addInsertedEntry();
         }
      }
      else {
         //
         // Updates entity only if previous value is forecast and value != 0
         //
         if (!entity.isActual() && value.compareTo(entity.getValue()) != 0) {
            entity.setActual(actual);
            entity.setTbdr(tbdr);
            entity.setValue(value);
            entity.setTime(new Time(time));

            financialService.save(entity);
            info.addUpdatedEntry();
         }
      }
   }
   
   private Financial filter(List<Financial> financials, String time, String category, String enterpriseId, boolean eac) {
      List<Financial> entities = null;
      
      if (eac) {
         entities = financials.stream()
                              .filter(financial -> financial.getCategory().equals(category))
                              .collect(Collectors.toList());
      }
      else {
         if (enterpriseId == null || enterpriseId.isEmpty()) {
            entities = financials.stream()
                                 .filter(financial -> financial.getTime().getId().equals(time))
                                 .filter(financial -> financial.getCategory().equals(category))
                                 .collect(Collectors.toList());
         }
         else {
            entities = financials.stream()
                                 .filter(financial -> financial.getTime().getId().equals(time))
                                 .filter(financial -> financial.getCategory().equals(category))
                                 .filter(financial -> financial.getEnterpriseId() != null)
                                 .filter(financial -> financial.getEnterpriseId().equals(enterpriseId))
                                 .collect(Collectors.toList());         
         }
      }
      
      if (!entities.isEmpty()) {
         return entities.get(0);
      }
      
      return null;
   }

   private String formatDateAsString(Date date) {
      DateFormat formatter = new SimpleDateFormat(ConstantsHelper.DEFAULT_TIMES_FORMAT);
      
      return formatter.format(date);
   }

   private List<PairIntegerString> getPoCDetailsCategories(Workbook workbook) {
      List<PairIntegerString> categories = new ArrayList<>();

      for (Row row : workbook.getSheet(ConstantsHelper.POC_DETAILS_SHEET_DATA)) {
         String value = helper.parseCellAsString(row, ConstantsHelper.EXCEL_COLUMN_B, ConstantsHelper.EXCEL_STRING_FORMAT);
         
         //
         // Skip categories parsing if value is empty
         //
         if (value.isEmpty())
            continue;
         
         for (String category : categoriesNames) {
            if (category.equals(value)) {
               categories.add(new PairIntegerString(row.getRowNum(), category));
            }
         }
      }

      return categories;
   }
   
   private List<PairIntegerDate> getPoCDetailsDates(Workbook workbook, Date min, Date max) {
      List<PairIntegerDate> dates = new ArrayList<>();
      
      for (Cell cell : workbook.getSheet(ConstantsHelper.POC_DETAILS_SHEET_DATA).getRow(POC_DETAILS_ROW_MONTHS)) {
         String value = helper.parseCellAsString(cell, ConstantsHelper.EXCEL_STRING_FORMAT);
         
         if (value.contains(ACTUAL) || value.contains(FORECAST)) {
            String[] splitted = value.split("\\s+");
            Date date = getDate("20" + splitted[1] + "-" + String.format("%02d", getMonthNumber(splitted[0])) + "-01", ConstantsHelper.DEFAULT_FORMAT_DATE);

            if (date != null && date.compareTo(min) >= 0 && date.compareTo(max) <= 0) {
               dates.add(new PairIntegerDate(cell.getColumnIndex(), date));
            }
         }
      }

      return dates;
   }
   
   private Date getDate(String date, String format) {
      SimpleDateFormat formatter = new SimpleDateFormat(format);
      Date result = null;
       
      try {
         result = formatter.parse(date);
      }
      catch (ParseException ex) {
         logger.error("An error has occured parsing date. Exception: {}", ex.getMessage());
      }
       
      return result;
   }

   private boolean isResourceTrendCategory(String category) {
      return category.equalsIgnoreCase(COST_RATE) || category.equalsIgnoreCase(HOURS) || category.equalsIgnoreCase(PAYROLL);
   }

   private boolean isSolutionContingencyCategory(String category) {
      return category.equalsIgnoreCase(REMAINING_BUDGET_AMT) || category.equalsIgnoreCase(USED_FOR_RISK_AMT) || category.equalsIgnoreCase(WRITE_UP_CI_AMT);
   }

   private int getMonthNumber(String monthName) {
      Date date;
      int month = 0;
      
      try {
         date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(monthName);

         Calendar cal = Calendar.getInstance();
         cal.setTime(date);
         month = cal.get(Calendar.MONTH) + 1;
      } 
      catch (ParseException e) {
         e.printStackTrace();
      }

      return month;
   }
}
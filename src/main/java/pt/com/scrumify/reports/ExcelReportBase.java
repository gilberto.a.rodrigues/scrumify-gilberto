package pt.com.scrumify.reports;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import pt.com.scrumify.helpers.ConstantsHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelReportBase {
   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PROTOCOL)
   private String protocol;
   @Value(ConstantsHelper.PROPERTIES_APPLICATION_DOMAIN)
   private String domain;
   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PORT)
   private String port;
   @Value(ConstantsHelper.PROPERTIES_APPLICATION_PATH)
   private String path;
   
   protected static final String REPORT_FONT_NAME = "Roboto Condensed";

   protected static final int COLUMN_A = 0;
   protected static final int COLUMN_B = 1;
   protected static final int COLUMN_C = 2;
   protected static final int COLUMN_D = 3;
   protected static final int COLUMN_E = 4;
   protected static final int COLUMN_F = 5;
   protected static final int COLUMN_G = 6;
   protected static final int COLUMN_H = 7;
   protected static final int COLUMN_I = 8;
   protected static final int COLUMN_J = 9;
   protected static final int COLUMN_K = 10;
   protected static final int COLUMN_L = 11;
   protected static final int COLUMN_M = 12;
   protected static final int COLUMN_N = 13;
   protected static final int COLUMN_O = 14;
   protected static final int COLUMN_P = 15;
   protected static final int COLUMN_Q = 16;
   protected static final int COLUMN_R = 17;
   protected static final int COLUMN_S = 18;
   protected static final int COLUMN_T = 19;
   protected static final int COLUMN_U = 20;
   protected static final int COLUMN_V = 21;
   protected static final int COLUMN_W = 22;
   protected static final int COLUMN_X = 23;
   protected static final int COLUMN_Y = 24;
   protected static final int COLUMN_Z = 25;
   
   protected static final int ROW_001 = 0;
   protected static final int ROW_002 = 1;
   protected static final int ROW_003 = 2;
   protected static final int ROW_004 = 3;
   protected static final int ROW_005 = 4;
   protected static final int ROW_006 = 5;
   protected static final int ROW_007 = 6;
   protected static final int ROW_008 = 7;
   protected static final int ROW_009 = 8;
   protected static final int ROW_010 = 9;
   protected static final int ROW_011 = 10;
   protected static final int ROW_012 = 11;
   protected static final int ROW_013 = 12;
   protected static final int ROW_014 = 13;
   protected static final int ROW_015 = 14;
   protected static final int ROW_016 = 15;
   protected static final int ROW_017 = 16;
   protected static final int ROW_018 = 17;
   protected static final int ROW_019 = 18;
   protected static final int ROW_020 = 19;
   protected static final int ROW_021 = 20;
   protected static final int ROW_022 = 21;
   protected static final int ROW_023 = 22;
   protected static final int ROW_024 = 23;
   protected static final int ROW_025 = 24;
   protected static final int ROW_026 = 25;
   protected static final int ROW_027 = 26;
   protected static final int ROW_028 = 27;
   protected static final int ROW_029 = 28;
   protected static final int ROW_030 = 29;
   protected static final int ROW_031 = 30;
   protected static final int ROW_032 = 31;
   protected static final int ROW_033 = 32;
   protected static final int ROW_034 = 33;
   protected static final int ROW_035 = 34;
   protected static final int ROW_036 = 35;
   protected static final int ROW_037 = 36;
   protected static final int ROW_038 = 37;
   protected static final int ROW_039 = 38;
   protected static final int ROW_040 = 39;
   protected static final int ROW_041 = 40;
   protected static final int ROW_042 = 41;
   protected static final int ROW_043 = 42;
   protected static final int ROW_044 = 43;
   protected static final int ROW_045 = 44;
   protected static final int ROW_046 = 45;
   protected static final int ROW_047 = 46;
   protected static final int ROW_048 = 47;
   protected static final int ROW_049 = 48;
   protected static final int ROW_050 = 49;
   protected static final int ROW_051 = 50;
   protected static final int ROW_052 = 51;
   protected static final int ROW_053 = 52;
   protected static final int ROW_054 = 53;
   protected static final int ROW_055 = 54;
   protected static final int ROW_056 = 55;
   protected static final int ROW_057 = 56;
   protected static final int ROW_058 = 57;
   protected static final int ROW_059 = 58;
   protected static final int ROW_060 = 59;
   protected static final int ROW_061 = 60;
   protected static final int ROW_062 = 61;
   protected static final int ROW_063 = 62;
   protected static final int ROW_064 = 63;
   protected static final int ROW_065 = 64;
   protected static final int ROW_066 = 65;
   protected static final int ROW_067 = 66;
   protected static final int ROW_068 = 67;
   protected static final int ROW_069 = 68;
   protected static final int ROW_070 = 69;
   protected static final int ROW_071 = 70;
   protected static final int ROW_072 = 71;
   protected static final int ROW_073 = 72;
   protected static final int ROW_074 = 73;
   protected static final int ROW_075 = 74;
   protected static final int ROW_076 = 75;
   protected static final int ROW_077 = 76;
   protected static final int ROW_078 = 77;
   protected static final int ROW_079 = 78;
   protected static final int ROW_080 = 79;
   protected static final int ROW_081 = 80;
   protected static final int ROW_082 = 81;
   protected static final int ROW_083 = 82;
   protected static final int ROW_084 = 83;
   protected static final int ROW_085 = 84;
   protected static final int ROW_086 = 85;
   protected static final int ROW_087 = 86;
   protected static final int ROW_088 = 87;
   protected static final int ROW_089 = 88;
   protected static final int ROW_090 = 89;
   protected static final int ROW_091 = 90;
   protected static final int ROW_092 = 91;
   protected static final int ROW_093 = 92;
   protected static final int ROW_094 = 93;
   protected static final int ROW_095 = 94;
   protected static final int ROW_096 = 95;
   protected static final int ROW_097 = 96;
   protected static final int ROW_098 = 97;
   protected static final int ROW_099 = 98;
   protected static final int ROW_100 = 99;
   
   protected List<XSSFColor> colors = new ArrayList<>();
   protected List<XSSFFont> fonts = new ArrayList<>();
   protected List<XSSFCellStyle> styles = new ArrayList<>();
   
   protected XSSFWorkbook workbook;
   protected XSSFSheet worksheet;
   protected int row;

   protected String getApplicationUrl() {
      return protocol + "://" + domain + (!port.equals("") ? ":" + port : "") + path;
   }
   
   public void init() {
      this.workbook = null;
      this.worksheet = null;
      colors.clear();
      fonts.clear();
      styles.clear();
      
      this.row = 0;
   }
   
   public void addColor(byte red, byte green, byte blue) {
      XSSFColor color = new XSSFColor(new byte[]{red, green, blue}, null);
      
      colors.add(color);
   }
   
   public void addFont(String fontName, short height, boolean bold, boolean italic, XSSFColor color) {
      XSSFFont font = workbook.createFont();
      font.setFontName(fontName);
      font.setFontHeightInPoints((short) height);
      font.setColor(color);
      font.setBold(bold);
      font.setItalic(italic);
      
      fonts.add(font);
   }
   
   public void addStyle(boolean wrapText, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, FillPatternType fillPattern, XSSFColor foregroundColor, XSSFFont font) {
      XSSFCellStyle style = workbook.createCellStyle();
      style.setAlignment(horizontalAlignment);
      style.setVerticalAlignment(verticalAlignment);
      style.setFillPattern(fillPattern);
      style.setFillForegroundColor(foregroundColor);
      style.setFont(font);
      style.setWrapText(wrapText);
      
      styles.add(style);
   }
   
   public void addStyle(boolean wrapText, HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment, FillPatternType fillPattern, XSSFColor foregroundColor, XSSFFont font, BorderStyle borderTop, XSSFColor borderTopFont, BorderStyle borderRight, XSSFColor borderRightFont, BorderStyle borderBottom, XSSFColor borderBottomFont, BorderStyle borderLeft, XSSFColor borderLeftFont) {
      XSSFCellStyle style = workbook.createCellStyle();
      style.setAlignment(horizontalAlignment);
      style.setVerticalAlignment(verticalAlignment);
      style.setFillPattern(fillPattern);
      style.setFillForegroundColor(foregroundColor);
      style.setFont(font);
      style.setWrapText(wrapText);
      if (borderTop != null) {
         style.setBorderTop(borderTop);
      }
      if (borderTopFont != null) {
         style.setTopBorderColor(borderTopFont);
      }
      if (borderRight != null) {
         style.setBorderRight(borderRight);
      }
      if (borderRightFont != null) {
         style.setRightBorderColor(borderRightFont);
      }
      if (borderBottom != null) {
         style.setBorderBottom(borderBottom);
      }
      if (borderBottomFont != null) {
         style.setBottomBorderColor(borderBottomFont);
      }
      if (borderLeft != null) {
         style.setBorderLeft(borderLeft);
      }
      if (borderLeftFont != null) {
         style.setLeftBorderColor(borderLeftFont);
      }
      
      styles.add(style);
   }
   
   public void write(int column, int row, XSSFCellStyle style, Date cellValue) {
      write(column, row, style, cellValue, null);
   }
   
   public void write(int column, int row, XSSFCellStyle style, Integer cellValue) {
      write(column, row, style, cellValue, null);
   }
   
   public void write(int column, int row, XSSFCellStyle style, String cellValue) {
      write(column, row, style, cellValue, null, null);
   }
   
   public void write(int column, int row, XSSFCellStyle style, Date cellValue, Float height) {
      XSSFRow r = this.worksheet.getRow(row);
      if (r == null) {
         r = this.worksheet.createRow(row);
      }
      
      if (height != null) {
         r.setHeightInPoints(height);
      }
      
      XSSFCell c = r.getCell(column);
      if (c == null) {
         c = r.createCell(column);
      }
      
      c.setCellStyle(style);
      c.setCellValue(cellValue);
   }
   
   public void write(int column, int row, XSSFCellStyle style, Integer cellValue, Float height) {
      XSSFRow r = this.worksheet.getRow(row);
      if (r == null) {
         r = this.worksheet.createRow(row);
      }
      
      if (height != null) {
         r.setHeightInPoints(height);
      }
      
      XSSFCell c = r.getCell(column);
      if (c == null) {
         c = r.createCell(column);
      }
      
      c.setCellStyle(style);
      c.setCellValue(cellValue);
   }
   
   public void write(int column, int row, XSSFCellStyle style, String cellValue, Float height, String url) {
      XSSFRow r = this.worksheet.getRow(row);
      if (r == null) {
         r = this.worksheet.createRow(row);
      }
      
      if (height != null) {
         r.setHeightInPoints(height);
      }
      
      XSSFCell c = r.getCell(column);
      if (c == null) {
         c = r.createCell(column);
      }
      
      c.setCellStyle(style);
      c.setCellValue(cellValue);
      
      if (url != null) {
         CreationHelper helper = this.workbook.getCreationHelper();
         
         Hyperlink link = helper.createHyperlink(HyperlinkType.URL);
         link.setAddress(getApplicationUrl() + url);
         link.setLabel(cellValue);
         c.setHyperlink(link);
      }
   }
}
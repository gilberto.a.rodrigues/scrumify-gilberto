package pt.com.scrumify.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pt.com.scrumify.database.services.AreaService;
import pt.com.scrumify.entities.AreaView;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class AreasController {
   @Autowired
   private AreaService areasService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * AREAS
    */
   @GetMapping(value = ConstantsHelper.MAPPING_AREAS)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_AREAS_READ + "')")
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, this.areasService.listAll());
      
      return ConstantsHelper.VIEW_AREAS_READ;
   }
 
   @GetMapping(value = ConstantsHelper.MAPPING_AREAS_CREATE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_AREAS_CREATE + "')")
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, new AreaView());

      return ConstantsHelper.VIEW_AREAS_CREATE;
   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_AREAS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_AREA)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_AREAS_UPDATE + "')")
   public String update(Model model, @PathVariable int area) {
      AreaView areaView = new AreaView(this.areasService.getOne(area));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, areaView);

      return ConstantsHelper.VIEW_AREAS_UPDATE;
   }   

   @PostMapping(value = ConstantsHelper.MAPPING_AREAS_SAVE)
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_AREAS_UPDATE + "')")
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA) @Valid AreaView areaView, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREA, areaView);
         
         return ConstantsHelper.VIEW_AREAS_UPDATE;
      }
      
      this.areasService.save(areaView.translate());

      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_AREAS;
   }
   
}
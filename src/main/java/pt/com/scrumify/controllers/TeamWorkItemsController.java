package pt.com.scrumify.controllers;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.*;
import pt.com.scrumify.entities.TeamView;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.WorkItemsHelper;

import javax.validation.Valid;


@Controller
public class TeamWorkItemsController {

   @Autowired
   private TeamService teamsService;
   @Autowired
   private WorkItemService workitemsService;
   @Autowired
   private WorkItemsHelper workitemsHelper;
   @Autowired
   private TypeOfWorkItemService typeOfWorkitemService;
   @Autowired
   private UserStoryService userStoriesService;
   @Autowired
   private ApplicationService appService;
     
   /*
    * TAB TEAM WORKITEMS
    */
   @ApiOperation("Mapping to respond ajax requests for Tab Team WorkItems.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping( value = ConstantsHelper.MAPPING_TEAMS_WORKITEMS_AJAX + ConstantsHelper.MAPPING_PARAMETER_TEAM )
   public String teamworkitemTab(Model model, @PathVariable int team ) {
      TeamView teamView = new TeamView(this.teamsService.getOne(team));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, workitemsService.getWorkItemsByTeamId(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());
      
      return ConstantsHelper.VIEW_TEAMS_WORKITEMS;
   }
   
   /*
    * ADD TEAM WORKITEMS
    */
   @ApiOperation("Mapping to add a work item to a team")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_WORKITEMS_CREATE + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMWORKITEMS + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_TYPEOFWORKITEM)
   public String add(Model model, @PathVariable int team, @PathVariable int typeOfWorkitem) {
      WorkItemView workitem =  new WorkItemView();
      workitem.setType(typeOfWorkitemService.getOne(typeOfWorkitem));
      workitem.setTeam(teamsService.getOne(team));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM, workitem);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, userStoriesService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, appService.listAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, workitem.getTeam());
     
      return ConstantsHelper.VIEW_TEAMWORKITEMS_CREATE;
   }
   
   /*
    * SAVE TEAM WORKITEMS
    */
   @ApiOperation("Mapping to save a Team WorkItem")
   @PostMapping(value = ConstantsHelper.MAPPING_TEAMWORKITEMS_SAVE)
   public String save(Model model, @Valid WorkItemView workitem) {
      WorkItem work = this.workitemsHelper.save(workitem);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMS, workitemsService.getWorkItemsByTeamId(work.getTeam().getId()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TYPESOFWORKITEM, typeOfWorkitemService.getAvailable());
      
           
      return ConstantsHelper.VIEW_TEAMS_WORKITEMS + " :: team-workitems";
      
   }
  
}
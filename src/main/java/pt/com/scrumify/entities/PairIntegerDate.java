package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class PairIntegerDate {
   @Getter
   @Setter
   private int index;
   
   @Getter
   @Setter
   private Date value;
   
   public PairIntegerDate(int index, Date value) {
      this.index = index;
      this.value = value;
   }
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.EpicNote;
import pt.com.scrumify.database.entities.TypeOfNote;

public class EpicNoteView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private Epic epic;
   
   @Getter
   @Setter
   private String content;   
   
   @Getter
   @Setter
   private TypeOfNote typeOfNote;

   public EpicNoteView() {
      super();
   }

   public EpicNoteView(EpicNote epicNote) {
      super();

      this.setId(epicNote.getId());     
      this.setTypeOfNote(epicNote.getTypeOfNote());
      this.setContent(epicNote.getContent());
      this.setEpic(epicNote.getEpic());

   }

   public EpicNote translate() {
      EpicNote epicNote = new EpicNote();

      epicNote.setId(id);      
      epicNote.setContent(content);
      epicNote.setTypeOfNote(typeOfNote);
      epicNote.setEpic(epic);

      return epicNote;
   }
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.ApplicationProperty;
import pt.com.scrumify.database.entities.ApplicationPropertyPK;

@NoArgsConstructor
public class ApplicationPropertyView {
   @Getter
   @Setter
   private ApplicationPropertyPK pk = new ApplicationPropertyPK();
   
   @Getter
   @Setter
   private String value;
   
   public ApplicationPropertyView(ApplicationProperty applicationProperty) {
      super();
      
      this.setPk(applicationProperty.getPk());
      this.setValue(applicationProperty.getValue());
   }

   public ApplicationProperty translate() {
      ApplicationProperty applicationProperty = new ApplicationProperty();
      
      applicationProperty.setPk(this.getPk());
      applicationProperty.setValue(this.getValue());
 
      return applicationProperty;  
   }  
}
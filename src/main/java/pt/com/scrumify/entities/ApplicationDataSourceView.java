package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.ApplicationDataSource;
import pt.com.scrumify.database.entities.Environment;

@NoArgsConstructor
public class ApplicationDataSourceView {
   @Getter
   @Setter
   private Integer id;

   @Getter
   @Setter
   private String name;
   
   @Getter
   @Setter
   private String type;
   
   @Getter
   @Setter
   private String dataBaseType;
   
   @Getter
   @Setter
   private String dataBaseName;
   
   @Getter
   @Setter
   private String hostname;
   
   @Getter
   @Setter
   private String port;
   
   @Getter
   @Setter
   private String username;
   
   @Getter
   @Setter
   private String password;

   @Getter
   @Setter
   private Application application = new Application();
   
   @Getter
   @Setter
   private Environment environment = new Environment();
   
   public ApplicationDataSourceView(ApplicationDataSource applicationDataSource) {
      super();
      
      this.setApplication(applicationDataSource.getApplication());
      this.setEnvironment(applicationDataSource.getEnvironment());
      this.setType(applicationDataSource.getType());
      this.setDataBaseType(applicationDataSource.getDatabaseType());
      this.setName(applicationDataSource.getName());
      this.setPassword(applicationDataSource.getPassword());
      this.setPort(applicationDataSource.getPort());
      this.setUsername(applicationDataSource.getUsername());
      this.setHostname(applicationDataSource.getHostname());
      this.setDataBaseName(applicationDataSource.getDatabaseName());
      this.setId(applicationDataSource.getId());  
   }

   public ApplicationDataSource translate() {
      ApplicationDataSource applicationDataSource = new ApplicationDataSource();
      
      applicationDataSource.setApplication(this.application);
      applicationDataSource.setEnvironment(this.environment);
      applicationDataSource.setDatabaseType(this.dataBaseType);
      applicationDataSource.setType(this.type);
      applicationDataSource.setName(this.name);
      applicationDataSource.setPassword(this.password);
      applicationDataSource.setPort(this.port);
      applicationDataSource.setUsername(this.username);
      applicationDataSource.setHostname(this.hostname);
      applicationDataSource.setId(this.id);     
      applicationDataSource.setDatabaseName(this.dataBaseName);      
      
      return applicationDataSource;  
   }
}
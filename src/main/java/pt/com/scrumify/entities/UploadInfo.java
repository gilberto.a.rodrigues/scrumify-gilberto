package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class UploadInfo {
   
   @Getter
   @Setter
   private String fileName;
   
   @Getter
   @Setter
   private String message = "";
   
   @Getter
   @Setter
   private int entriesInserted = 0;
   
   @Getter
   @Setter
   private int entriesUpdated = 0;
   
   @Getter
   @Setter
   private long size = 0;
   
   @Getter
   @Setter
   private double time = 0;
   
   public UploadInfo(String fileName, String message) {
      this.fileName = fileName;
      this.message = message;
   }
   
   public UploadInfo(String fileName) {
      this.fileName = fileName;
   }
   
   public UploadInfo(String fileName, long size) {
      this.fileName = fileName;
      this.size = size;
   }
   
   public void addInsertedEntry() {
      this.entriesInserted += 1;
   }
   
   public void addUpdatedEntry() {
      this.entriesUpdated += 1;
   }
}
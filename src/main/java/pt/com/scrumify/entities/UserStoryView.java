package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

public class UserStoryView {

   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   private String name;

   @Getter
   @Setter
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private Integer estimate = 0;

   @Getter
   @Setter
   private String description;

   @Getter
   @Setter
   private WorkItemStatus status;

   @Getter
   @Setter
   private TypeOfWorkItem type;

   @Getter
   @Setter
   private Epic epic;

   @Getter
   @Setter
   private Team team;

   @Getter
   @Setter
   private List<WorkItem> workitems;

   @Getter
   @Setter
   private List<UserStoryNote> notes;

   @Getter
   @Setter
   private List<Tag> tags;

   @Getter
   @Setter
   private String number;

   @Getter
   @Setter
   private Integer hours;

   @Getter
   @Setter
   private Resource createdBy;

   @Getter
   @Setter
   private Date created;

   public UserStoryView() {
      super();
   }

   public UserStoryView(UserStory userStory) {
      super();

      this.setId(userStory.getId());
      this.setName(userStory.getName());
      this.setDescription(userStory.getDescription());
      this.setStatus(userStory.getStatus());
      this.setType(userStory.getType());
      this.setEpic(userStory.getEpic());
      this.setWorkitems(userStory.getWorkItems());
      this.setNotes(userStory.getNotes());
      this.setTeam(userStory.getTeam());
      this.setEstimate(userStory.getEstimate());
      this.setNumber(userStory.getNumber());
      this.setHours(userStory.getHours());
      this.setCreated(userStory.getCreated());
      this.setCreatedBy(userStory.getCreatedBy());
      this.setTags(userStory.getTags());
   }

   public UserStory translate() {
      UserStory userStory = new UserStory();

      userStory.setId(id);
      userStory.setName(name);
      userStory.setDescription(description);
      userStory.setStatus(status);
      userStory.setType(type);
      userStory.setEpic(epic);
      userStory.setWorkItems(workitems);
      userStory.setNotes(notes);
      userStory.setTeam(team);
      userStory.setEstimate(estimate);
      userStory.setTags(tags);
      userStory.setNumber(number);
      userStory.setHours(hours);
      userStory.setCreated(created);
      userStory.setCreatedBy(createdBy);

      return userStory;
   }
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Time;

public class TimeReportView {
   
   @Getter
   @Setter
   private Time day;
   
   @Getter
   @Setter
   private String subArea;
   
   @Getter
   @Setter
   private String contractName;
   
   @Getter
   @Setter
   private String workitemName;
   
   @Getter
   @Setter
   private long timeSpent;
      
   public TimeReportView() {
      super();
   }
   
   public TimeReportView(Time data, String subArea, String contractName, String workitemName, long timeSpent) {
      super();
      
      this.day = data;
      this.subArea = subArea;
      this.contractName = contractName;
      this.workitemName = workitemName;
      this.timeSpent = timeSpent;
   }
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class EpicFilterView {
   @Getter
   @Setter
   private List<ItemStatusView> statuses;
   
   @Getter
   @Setter
   String searchQuery;
   
   public EpicFilterView() {
      super();
   }
   
   public EpicFilterView(List<ItemStatusView> statuses) {
      super();
      
      this.statuses = statuses; 
   }
   
   public void addStatus(ItemStatusView status) {
      if(this.statuses == null)
         this.statuses = new ArrayList<>();
      
      statuses.add(status);
   }
   
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SignUp {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(min=3, max=3, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String abbreviation;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Gender gender = new Gender(Gender.MALE);
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Email(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMAIL + "}")
   private String email;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private Avatar avatar;

   public Resource translate() {
      Resource resource = new Resource();
      
      resource.setId(this.getId());
      resource.setAbbreviation(this.getAbbreviation());
      resource.setName(this.getName());
      resource.setEmail(this.getEmail());
      resource.setGender(this.getGender());
      resource.setAvatar(this.getAvatar());
      
      return resource;
   }
}
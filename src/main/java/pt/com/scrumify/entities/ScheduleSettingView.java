package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.ScheduleSetting;

import java.util.Date;

public class ScheduleSettingView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private Schedule schedule;
   
   @Getter
   @Setter   
   private Date startingDay;
   
   @Getter
   @Setter
   private Date endingDay;
   
   @Getter
   @Setter
   private Integer monday;
   
   @Getter
   @Setter
   private Integer tuesday;
   
   @Getter
   @Setter
   private Integer wednesday;
   
   @Getter
   @Setter
   private Integer thursday;
   
   @Getter
   @Setter
   private Integer friday;
   
   @Getter
   @Setter
   private Integer saturday;
   
   @Getter
   @Setter
   private Integer sunday;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   @Getter
   @Setter
   private Date created;
   
   public ScheduleSettingView() {
      super();
   }
   
   public ScheduleSettingView(ScheduleSetting setting) {
      super();
      
      this.setId(setting.getId());
      this.setSchedule(setting.getSchedule());
      this.setEndingDay(setting.getEndingDay().getDate());
      this.setFriday(setting.getFriday());
      this.setMonday(setting.getMonday());
      this.setSaturday(setting.getSaturday());
      this.setStartingDay(setting.getStartingDay().getDate());
      this.setSunday(setting.getSunday());
      this.setThursday(setting.getThursday());
      this.setTuesday(setting.getTuesday());
      this.setWednesday(setting.getWednesday());
      this.setCreated(setting.getCreated());
      this.setCreatedBy(setting.getCreatedBy());

   }

   public ScheduleSetting translate() {
      ScheduleSetting setting = new ScheduleSetting();
      
      setting.setId(id);
      setting.setFriday(friday);
      setting.setMonday(monday);
      setting.setSaturday(saturday);
      setting.setSunday(sunday);
      setting.setThursday(thursday);
      setting.setTuesday(tuesday);
      setting.setWednesday(wednesday);
      setting.getStartingDay().setDate(startingDay);
      setting.getEndingDay().setDate(endingDay);
      setting.setSchedule(schedule);
      setting.setCreated(created);
      setting.setCreatedBy(createdBy);
      
      return setting;
   }
}
package pt.com.scrumify.entities;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.constraints.NotBlank;

public class AreaView {
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=4, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String abbreviation;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=50, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;
   
   public AreaView() {
      super();
   }
   
   public AreaView(Area area) {
      super();
      
      if (area != null) {
         this.setId(area.getId());
         this.setAbbreviation(area.getAbbreviation());
         this.setName(area.getName());
      }
   }
   
   public Area translate() {
      
      Area area = new Area();

      area.setId(this.getId());
      area.setAbbreviation(this.getAbbreviation());
      area.setName(this.getName());
      
      return area;
   }
}
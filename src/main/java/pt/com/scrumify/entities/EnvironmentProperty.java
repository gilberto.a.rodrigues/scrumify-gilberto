package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class EnvironmentProperty {
   
   @Getter
   @Setter
   private String propertyGroup;
   
   @Getter
   @Setter
   private String propertyName;
   
   public EnvironmentProperty (String propertyGroup, String propertyName) {
      this.propertyGroup = propertyGroup;
      this.propertyName = propertyName;
   }
}
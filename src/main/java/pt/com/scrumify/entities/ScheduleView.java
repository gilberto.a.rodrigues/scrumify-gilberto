package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.constraints.NotBlank;
import java.util.Date;

public class ScheduleView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
   
   @Getter
   @Setter
   private Boolean compensationDays = false;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource createdBy;
   
   public ScheduleView() {
      super();
   }
   
   public ScheduleView(Schedule schedule) {
      super();
      
      this.setId(schedule.getId());
      this.setName(schedule.getName());
      this.setCompensationDays(schedule.getCompensationDays());
      this.setCreated(schedule.getCreated());
      this.setCreatedBy(schedule.getCreatedBy());
   }
   
   public Schedule translate() {
      Schedule schedule = new Schedule();
      
      schedule.setId(id);
      schedule.setCompensationDays(compensationDays);
      schedule.setName(name);
      schedule.setCreated(created);
      schedule.setCreatedBy(createdBy);
      
      return schedule;
   }
   
}
package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import pt.com.scrumify.database.entities.Activity;
import pt.com.scrumify.database.entities.ProjectPhase;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class ActivityView {
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=10, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String number;
   
   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   @Length(max=80, message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_LENGTH + "}")
   private String name;
   
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private ProjectPhase projectPhase = new ProjectPhase();

   @Getter
   @Setter
   private Date created;

   @Getter
   @Setter
   private Resource createdBy;
   
   public ActivityView() {
      super();
   }
   
   public ActivityView(Activity activity) {
      super();
      
      if (activity != null) {
         this.setId(activity.getId());
         this.setNumber(activity.getNumber());
         this.setName(activity.getName());
         this.setProjectPhase(activity.getProjectPhase());
         this.setCreated(activity.getCreated());
         this.setCreatedBy(activity.getCreatedBy());
      }
   }
   
   public Activity translate() {
      Activity activity = new Activity();

      activity.setId(this.getId());
      activity.setNumber(this.getNumber());
      activity.setName(this.getName());
      activity.setProjectPhase(this.getProjectPhase());
      activity.setCreated(this.getCreated());
      activity.setCreatedBy(this.getCreatedBy());
      
      return activity;
   }
}
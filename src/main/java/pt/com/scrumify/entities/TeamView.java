package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TeamView {
   
   @Getter
   @Setter
   private Integer id = 0;

   @Getter
   @Setter
   @NotBlank(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY + "}")
   private String name;
      
   @Getter
   @Setter
   @Valid
   @NotNull(message = "{" + ConstantsHelper.MESSAGE_VALIDATION_EMPTY_CHOICE + "}")
   private SubArea subArea = new SubArea();
   
   @Getter
   @Setter
   private List<TeamMember> members = new ArrayList<>(0);
   
   @Getter
   @Setter
   private List<TeamContract> contracts = new ArrayList<>(0);
   
   @Getter
   @Setter
   private List<WorkItem> workitems = new ArrayList<>(0);

   @Getter
   @Setter
   private boolean technicalAssistance;
   
   @Getter
   @Setter
   private boolean active;
   
   @Getter
   @Setter
   private Date created;
   
   @Getter
   @Setter
   private Resource createdBy;

   public TeamView() {
      super();
   }

   public TeamView(Team team) {
      super();
      
      if (team != null) {
       this.setId(team.getId());
       this.setName(team.getName());
       this.setSubArea(team.getSubArea());
       this.setMembers(team.getMembers());
       this.setContracts(team.getContracts());
       this.setWorkitems(team.getWorkItems());
       this.setTechnicalAssistance(team.isTechnicalAssistance());
       this.setActive(team.isActive());
       this.setCreated(team.getCreated());
       this.setCreatedBy(team.getCreatedBy());
      }
   }

   public Team translate() {
      Team team = new Team();
      
      team.setId(this.getId());
      team.setName(this.getName());
      team.setSubArea(this.getSubArea());
      team.setMembers(this.getMembers());
      team.setContracts(this.getContracts());
      team.setWorkItems(this.getWorkitems());
      team.setTechnicalAssistance(this.isTechnicalAssistance());
      team.setActive(this.isActive());
      team.setCreated(this.getCreated());
      team.setCreatedBy(this.getCreatedBy());
      return team;
   }

}
package pt.com.scrumify.helpers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import pt.com.scrumify.entities.EnvironmentProperty;

import java.util.ArrayList;
import java.util.List;

public class ApplicationPropertiesHelper {
   private static final int LOG_MESSAGE_PAD = 50;
   
   private ConfigurableApplicationContext context;
   
   public ConfigurableApplicationContext getContext() {
      return context;
   }

   public void setContext(ConfigurableApplicationContext context) {
      this.context = context;
   }

   public void printStartUpLog(Logger logger) {
      logger.info("");
      logger.info(this.getMessageTitle("Server Settings"));
      logger.info("");
      logger.info(this.getMessageProperty("Processors (cores)", Runtime.getRuntime().availableProcessors()));
      logger.info(this.getMessagePropertyWithUnit("Memory - Maximum", Runtime.getRuntime().maxMemory() / 1024 / 1024, "Mb"));
      logger.info(this.getMessagePropertyWithUnit("Memory - Allocated", Runtime.getRuntime().totalMemory() / 1024 / 1024, "Mb"));
      logger.info(this.getMessagePropertyWithUnit("Memory - Free", Runtime.getRuntime().freeMemory() / 1024 / 1024, "Mb"));

      String propertyGroup = "";
      
      logger.info("");
      logger.info(this.getMessageTitle("Application Properties"));
      
      for (EnvironmentProperty property : this.getEnvironmentProperties()) {
         /*
          * In case of new group, write a blank line to log
          */
         if (!propertyGroup.equals(property.getPropertyGroup())) {
            logger.info("");
            logger.info(this.getMessageTitle(property.getPropertyGroup()));
            
            propertyGroup = property.getPropertyGroup();
         }
         
         logger.info(this.getMessageProperty(property.getPropertyName(), this.context.getEnvironment().getProperty(property.getPropertyName())));
      }
   }
   
   private List<EnvironmentProperty> getEnvironmentProperties() {
      List<EnvironmentProperty> properties = new ArrayList<>();

      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_SERVER, "server.port"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_SERVER, "server.servlet.context-path"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_PROFILE, "spring.profiles.active"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_DATA_SOURCE, "spring.datasource.driverClassName"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_DATA_SOURCE, "spring.datasource.initialize"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_DATA_SOURCE, "spring.datasource.url"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_DATA_SOURCE, "spring.datasource.username"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.active"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.host"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.port"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.username"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.properties.mail.smtp.auth"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.properties.mail.smtp.starttls.enable"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_MAIL, "spring.mail.sender"));      
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_JPA_HIBERNATE, "spring.jpa.show-sql"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_JPA_HIBERNATE, "spring.jpa.hibernate.ddl-auto"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_JPA_HIBERNATE, "spring.jpa.properties.hibernate.format_sql"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_JPA_HIBERNATE, "spring.jpa.properties.hibernate.show_sql"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_JPA_HIBERNATE, "spring.jpa.properties.hibernate.dialect"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LIQUIBASE, "spring.liquibase.change-log"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LIQUIBASE, "spring.liquibase.drop-first"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LIQUIBASE, "spring.liquibase.enabled"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_APPLICATION, "spring.main.banner-mode"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_INTERNATIONALIZATION, "spring.messages.cache-duration"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_THYMELEAF, "spring.thymeleaf.cache"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LOGGING, "logging.file"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LOGGING, "logging.level.org.springframework"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LOGGING, "logging.level.org.hibernate"));
      properties.add(new EnvironmentProperty(ConstantsHelper.ENVIRONMENT_PROPERTY_GROUP_LOGGING, "logging.level.pt.com.scrumify"));
      
      return properties;
   }
   
   private String getMessageTitle(String message) {
      return message.toUpperCase();
   }
   
   private String getMessageProperty(String property, long propertyValue) {
      return String.format("  %s : %d", StringUtils.rightPad(property, LOG_MESSAGE_PAD), propertyValue);
   }
   
   private String getMessageProperty(String property, String propertyValue) {
      return String.format("  %s : %s", StringUtils.rightPad(property, LOG_MESSAGE_PAD), propertyValue);
   }
   
   private String getMessagePropertyWithUnit(String property, long propertyValue, String propertyUnit) {
      return String.format("  %s : %d %s", StringUtils.rightPad(property, LOG_MESSAGE_PAD), propertyValue, propertyUnit);
   }
}
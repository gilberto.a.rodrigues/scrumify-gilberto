package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.database.services.*;
import pt.com.scrumify.entities.TeamView;

import java.util.List;

@Service("TeamsHelper")
public class TeamsHelper {
   @Autowired
   private ContractService contractsService;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TeamMemberService teamMembersService;
   @Autowired
   private UserStoryService userstoriesService;
   @Autowired
   private WorkItemService workitemsService;
   
   public void save(TeamView teamView){
      Team team = this.teamsService.save(teamView.translate());
      
      TeamMemberPK teamMemberPK = new TeamMemberPK();
      teamMemberPK.setTeam(team);
      teamMemberPK.setResource(ResourcesHelper.getResource());
      
      TeamMember teamMember = new TeamMember();
      Occupation occupation = new Occupation();
      occupation.setId(3);
      teamMember.setPk(teamMemberPK);
      teamMember.setOccupation(occupation);
      this.teamMembersService.save(teamMember);
   }
     
   
   public List<Resource> listOfResources(Integer id) {
      Team team = this.teamsService.getOne(id);
     
      List<Resource> resources   = this.resourcesService.getNonSystemUsersOrderByProfileAndResourceName();
      List<Resource> members     = this.resourcesService.getByTeam(team);
      resources.removeAll(members);
      
      return resources; 
   }
   
   public List<Contract> listOfContracts(Integer id) {
      Team team = this.teamsService.getOne(id);
      
      List<Contract> contracts      = this.contractsService.getBySubareaAndStatus(team.getSubArea() , ContractStatus.OPEN);
      List<Contract> teamcontracts  = this.contractsService.getByTeam(team);
      contracts.removeAll(teamcontracts);
      
      return contracts; 
   }
 
    public List<Team> teamsList(Integer id) {
    List<Team> teams = this.teamsService.getAll();
    teams.removeAll(this.teamsService.getByMember(this.resourcesService.getOne(id)));   
    
    return teams; 
   }   
    
    public boolean existDependencies(Team team) {
       return (!workitemsService.getWorkItemsByTeam(team).isEmpty() ||
               !epicsService.getByTeamAndNotFinalStatus(team).isEmpty() ||
               !userstoriesService.getByTeamAndNotFinalStatus(team).isEmpty())
             ;
    }
    
    public List<Team> getBasedOnActiveFlag(String activeFlag) {
       if("1".equals(activeFlag)) {
          return teamsService.getByMember(ResourcesHelper.getResource());
       } else {
          return teamsService.getByMemberAndInactive(ResourcesHelper.getResource());
       }
    }
   
}
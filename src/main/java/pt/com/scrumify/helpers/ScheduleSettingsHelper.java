package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.ScheduleSetting;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.services.ScheduleSettingService;
import pt.com.scrumify.database.services.TimeService;

import java.util.List;



@Service
public class ScheduleSettingsHelper {
   
   @Autowired
   private ScheduleSettingService service;
   @Autowired
   private TimeService timesService;
   
   @Autowired
   private TimesHelper timesHelper;
   
   
   public void save(ScheduleSetting setting) {
      
      service.save(setting);
      
      Time di = this.timesService.getOne(setting.getStartingDay().getId());
      Time df = this.timesService.getOne(setting.getEndingDay().getId());
      
      List<Time> times = timesService.getBetween(di, df);
      
      for (Time time : timesHelper.updateByScheduleSetting(times, setting)) {
         timesService.save(time);
      }
   }
}
package pt.com.scrumify.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.database.services.*;
import pt.com.scrumify.entities.ItemStatusView;
import pt.com.scrumify.entities.TypeOfWorkItemView;
import pt.com.scrumify.entities.WorkItemFilterView;
import pt.com.scrumify.entities.WorkItemView;
import pt.com.scrumify.services.EmailService;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Service("WorkItemsHelper")
public class WorkItemsHelper {
   @Autowired
   private CookiesHelper cookiesHelper;
   @Autowired
   private ContextHelper contextHelper;
   @Autowired
   private EmailService emailService;
   @Autowired
   private GeneralHelper generalHelper;
   @Autowired
   private EntityManager entityManager;
   @Autowired
   private ActivityService activityService;
   @Autowired
   private ApplicationService applicationService;
   @Autowired
   private ContractService contractService;
   @Autowired
   private PriorityService priorityService;
   @Autowired
   private ReleaseService releasesService;
   @Autowired
   private ResourceService resourceService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TypeOfWorkItemService typeOfWorkitemsService;
   @Autowired
   private UserStoryService userStoriesService;
   @Autowired
   private WorkItemService workItemService;
   @Autowired
   private WorkItemHistoryService workItemsHistoryService;
   @Autowired
   private WorkItemStatusService itemStatusService;
   
   private static final Logger logger = LoggerFactory.getLogger(WorkItemsHelper.class);
   
   @Transactional
   public WorkItem updateApplication(int workItem, int application) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setApplication(new Application(application));

      return save(db, copy);
   }

   @Transactional
   public WorkItem updateAssignedTo(int workItem, int resource) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      if (resource == 0) {
         copy.setAssigned(null);
         copy.setAssignedTo(null);
      }
      else {
         copy.setAssignedTo(new Resource(resource));
         copy.setAssigned(DatesHelper.now());
      }
      
      return save(db, copy);
   }

   @Transactional
   public WorkItem updateContract(int workItem, int contract) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setContract(new Contract(contract));

      return save(db, copy);
   }
   
   @Transactional
   public WorkItem updateEstimate(int workItem, int estimate) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setEstimate(estimate);

      return save(db, copy);
   }

   @Transactional
   public WorkItem updateField(WorkItemView view, String field) {
      WorkItem db = workItemService.getOne(view.getId());
      WorkItem copy = db.clone();
      
      switch (field) {
         case "name" :
            copy.setName(view.getName());
            break;
         case "description" :
            copy.setDescription(view.getDescription());
            break;
         default :
            break;
      }

      return save(db, copy);
   }

   @Transactional
   public WorkItem updatePriority(int workItem, int priority) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setPriority(new Priority(priority));

      return save(db, copy);
   }

   @Transactional
   public WorkItem updateRelease(int workItem, int release) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setRelease(new Release(release));

      return save(db, copy);
   }

   @Transactional
   public WorkItem updateStatus(int workItem, int status, HttpServletRequest request, HttpServletResponse response) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      WorkItemStatus nextStatus = itemStatusService.getOne(status);
      copy.setStatus(nextStatus);

      /*
       * If next status is different of current one and the configuration unassign is true remote assignment
       */
      if (db.getStatus().getId() != status && nextStatus.getUnassign()) {
         copy.setAssigned(null);
         copy.setAssignedTo(null);
      }
      
      /*
       * If next status is a final status, set closed and closedby
       */
      if (nextStatus.isFinalStatus()) {
         copy.setClosed(DatesHelper.now());
         copy.setClosedBy(ResourcesHelper.getResource());
      }
      
      WorkItem saved = save(db, copy);
      
      if (nextStatus.getNotify()) {
         emailService.sendWorkItemsNotificationStatus(saved, resourceService.getByTeamExceptCurrentResource(saved.getTeam(), ResourcesHelper.getResource()), request, response);
      }
      
      return saved;
   }

   @Transactional
   public WorkItem updateTeam(int workItem, int team) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      copy.setTeam(new Team(team));

      return save(db, copy);
   }

   @Transactional
   public WorkItem updateUserStory(int workItem, int userStory) {
      WorkItem db = workItemService.getOne(workItem);
      WorkItem copy = db.clone();

      UserStory us = userStoriesService.getOne(userStory);
      copy.setUserStory(us);
      copy.setTeam(us.getTeam());

      return save(db, copy);
   }

   /*
    * This method save workItem and history when 'actual' is not null. Not transactional, please ensure that you
    * put @Transactional annotation in upper method.
    */
   public WorkItem save(WorkItem actual, WorkItem workItem) {
      if (actual != null) {
         workItemsHistoryService.save(new WorkItemHistory(actual));
      }

      return workItemService.save(workItem);
   }

   @Transactional
   public WorkItem save(WorkItemView workItemView) {
      WorkItem workItem = null;

      if (workItemView.getId() == 0) {
         workItemView.setNumber(formatWorkItemNumber(workItemView.getType()));
         workItem = workItemView.translate();
         workItem.setStatus(itemStatusService.getOneByTypeOfWorkitem(workItemView.getType().getId()));
      } else {
         workItem = updateWorkitem(workItemView);
      }

      return workItemService.save(workItem);
   }
   
   public WorkItem updateWorkitem(WorkItemView workItemView) {
      WorkItem workItem = workItemService.getOne(workItemView.getId());

      workItem.setContract(
            (workItemView.getContract() != null && workItemView.getContract().getId() != 0) ? contractService.getOne(workItemView.getContract().getId()) : workItem.getContract());
      if ((workItem.getAssignedTo() != null && workItemView.getAssignedTo().getId() != 0)
            || (workItem.getAssignedTo() != null && !workItemView.getAssignedTo().getId().equals(workItem.getAssignedTo().getId()))) {
         workItem.setAssigned(DatesHelper.now());
      }
      workItem.setAssignedTo(workItemView.getAssignedTo() != null && workItemView.getAssignedTo().getId() != 0
            ? resourceService.getOne(workItemView.getAssignedTo().getId())
            : workItem.getAssignedTo());
      if (workItemView.getStatus() != null)
         workItem.setStatus(
               workItemView.getStatus().getId() != 0 ? itemStatusService.getOne(workItemView.getStatus().getId()) : workItem.getStatus());
      return workItem;
   }

   public String formatWorkItemNumber(TypeOfWorkItem type) {
      String format = "%06d";
      return type.getAbbreviation().substring(0, 3) + DatesHelper.year().toString().substring(2, 4) + String.format(format, workItemService.countByTypeOfWorkitem(type) + 1);
   }

   public List<Resource> listOfResourcesToAssign(WorkItem workItem) {
      List<Resource> resources = new ArrayList<>();
      
      if (isReviewerOrApprover(workItem)) {
         List<Team> teams = new ArrayList<>();
         teams.add(workItem.getTeam());
         resources = resourceService.getByTeams(teams);
      }
      else {
         resources.add(ResourcesHelper.getResource());
      }
//      if (workItem.getTeam() != null) {
//         List<Team> teams = new ArrayList<>();
//         teams.add(workItem.getTeam());
//         resources = resourceService.getByTeams(teams);
//      } else {
//         resources = resourceService.getByTeams(teamsService.getByMember(ResourcesHelper.getResource()));
//      }
//      if (!resources.remove(workItem.getAssignedTo())) {
//         workItem.setAssigned(null);
//         workItem.setAssignedTo(null);
//      }
      return resources;
   }

   public List<Contract> listOfContract(WorkItem workItem) {
      List<Contract> contracts = new ArrayList<>();

      if (workItem.getTeam() != null) {
         contracts = contractService.getByTeamAndStatus(workItem.getTeam(), ContractStatus.OPEN);
         contracts.remove(workItem.getContract());
      }

      return contracts;
   }

   public List<Team> listOfTeams(WorkItem workItem) {
      List<Team> teams = teamsService.getByMember(ResourcesHelper.getResource());
      teams.remove(workItem.getTeam());

      return teams;
   }

   public List<UserStory> listOfUserStory(WorkItem workItem) {
      List<UserStory> userStories = new ArrayList<>();

      if (workItem.getTeam() != null) {
         userStories = this.userStoriesService.getByTeam(workItem.getTeam());
         userStories.remove(workItem.getUserStory());
      }

      return userStories;
   }

   public List<Application> getListOfApplications(WorkItem workItem) {
      List<Application> applications = applicationService.listAllByResource(ResourcesHelper.getResource());
      applications.remove(workItem.getApplication());

      return applications;
   }

   public List<Release> getListOfReleases(WorkItem workItem) {
      List<Release> releases = new ArrayList<>();

      if (workItem.getApplication() != null) {
         releases = releasesService.getByApplication(workItem.getApplication().getId());
         releases.remove(workItem.getRelease());
      }

      return releases;
   }

   public void generateModel(Model model, WorkItem workItem) {
      Resource resource = ResourcesHelper.getResource();
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE, resource);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, listOfResourcesToAssign(workItem));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_APPLICATIONS, getListOfApplications(workItem));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, listOfContract(workItem));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PRIORITIES, priorityService.getAll());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_STATUS, itemStatusService.getByWorkflow(workItem.getType().getWorkflow(), workItem.getStatus()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_USERSTORIES, listOfUserStory(workItem));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RELEASES, getListOfReleases(workItem));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMemberExceptActual(resource, workItem.getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ACTIVITIES, activityService.getByTechnicalAssistance(workItem.getTeam().isTechnicalAssistance()));
   }

   public boolean editable(Integer workitem, String field) {
      boolean result = false;

      WorkItem wi = this.workItemService.getOne(workitem);
      if (wi != null) {
         switch (field) {
            case "name":
            case "description":
               if ((isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               break;
            case "estimate":
               if (wi.getEstimate() == 0 && (isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               break;
         }
      }
      
      return result;
   }
   
   public boolean editable(Integer workitem, String field, List<Object> items) {
      boolean result = false;
      
      if (items == null || items.isEmpty()) {
         return false;
      }
      
      WorkItem wi = this.workItemService.getOne(workitem);
      if (wi != null) {
         switch (field) {
            case "priority":
               if ((isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()){
                  result = true;
               }
               if (isReviewerOrApprover(wi) && wi.getStatus().isFinalStatus() && wi.getPriority() == null){
                  result = true;
               }
               break;
            case "application":
               if (!wi.getStatus().isInitialStatus() && (isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               if (isReviewerOrApprover(wi) && wi.getStatus().isFinalStatus() && wi.getApplication() == null){
                  result = true;
               }
               break;
            case "contract":
               if ((isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               if (isReviewerOrApprover(wi) && wi.getStatus().isFinalStatus() && wi.getContract() == null){
                  result = true;
               }
               break;
            case "userstory":
               if (!wi.getStatus().isInitialStatus() && (isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               if (isReviewerOrApprover(wi) && wi.getStatus().isFinalStatus() && wi.getUserStory() == null){
                  result = true;
               }
               break;
            case "release":
               if (!wi.getStatus().isInitialStatus() && (isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               if (isReviewerOrApprover(wi) && wi.getStatus().isFinalStatus() && wi.getRelease() == null){
                  result = true;
               }
               break;
            case "assignedto":
               if ((wi.getWorkLogs().isEmpty()) && (wi.getAssignedTo() == null || isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               break;
            case "status":
               if (wi.getEstimate() > 0 && (isCreatedByOrAssignedTo(wi) || isReviewerOrApprover(wi)) && !wi.getStatus().isFinalStatus()) {
                  result = true;
               }
               break;
         }
      }
      
      return result;
   }
   
   private boolean isCreatedByOrAssignedTo(WorkItem wi) {
      if (wi.getCreatedBy().getId().equals(ResourcesHelper.getResource().getId())) {
         return true;
      }
      
      if (wi.getAssignedTo() != null && wi.getAssignedTo().getId().equals(ResourcesHelper.getResource().getId())) {
         return true;
      }
      
      return false;
   }
   
   private boolean isReviewerOrApprover(WorkItem wi) {
      for (TeamMember member : wi.getTeam().getMembers()) {
         /*
          * The logged user is approver or reviewer of the workitem team
          */
         if (member.getResource().getId().equals(ResourcesHelper.getResource().getId()) && (member.isApprover() || member.isReviewer())) {
            return true;
         }
      }
      
      return false;
   }

   public Map<TypeOfWorkItem, List<WorkItemStatus>> generateFilters() {
      Map<TypeOfWorkItem, List<WorkItemStatus>> map = new LinkedHashMap<>();

      for (TypeOfWorkItem type : typeOfWorkitemsService.getAvailable()) {
         List<WorkItemStatus> statuses = itemStatusService.getOneByTypeOfWorkitemById(type.getId());
         map.put(type, statuses);
      }

      return map;
   }

   public WorkItemFilterView generateFilter() {
      WorkItemFilterView filter = new WorkItemFilterView();

      for (TypeOfWorkItem type : typeOfWorkitemsService.getAvailable()) {
         List<WorkItemStatus> statuses = itemStatusService.getOneByTypeOfWorkitemById(type.getId());
         if (statuses != null && !statuses.isEmpty()) {
            type.setStatuses(statuses);
            filter.addType(new TypeOfWorkItemView(type));
         }
      }

      return filter;
   }

   public List<WorkItem> fuzzySearch(String searchTerm) {

      FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
      QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(WorkItem.class).get();

      Query luceneQuery = qb.keyword().fuzzy().withEditDistanceUpTo(1).onFields("name", "description", "number").matching(searchTerm).createQuery();

      javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, WorkItem.class);

      List<WorkItem> workItems = new ArrayList<>();
      try {
         workItems = generalHelper.castList(WorkItem.class, jpaQuery.getResultList());

      } catch (NoResultException nre) {
         // do nothing.
      }
      return workItems;
   }

   public List<WorkItem> getByFilter(WorkItemFilterView filter) {
      List<WorkItem> returnList = new ArrayList<>();

      if (filter.getTypes() != null && filter.getTypes().stream().anyMatch(e -> e.getId() != null)) {
         StringBuilder queryBuilder = new StringBuilder("SELECT DISTINCT w FROM WorkItem w " + "JOIN w.team t " + "JOIN t.members tm " + "WHERE tm.pk.resource = :resource ");

         boolean firstEntry = true;
         for (TypeOfWorkItemView type : filter.getTypes()) {
            if (type.getName() != null && type.getId() != null) {
               if (!firstEntry)
                  queryBuilder.append(" OR ");
               else {
                  firstEntry = false;
                  queryBuilder.append(" AND (");
               }
               queryBuilder.append(" (w.type.id = " + type.getId());
               fillInStatuses(queryBuilder, type);
            }
         }
         queryBuilder.append(")");
         
         if (filter.getOption() == 1)
            queryBuilder.append(" AND w.assignedTo = :resource ");
         else if(filter.getOption() == 2)
            queryBuilder.append(" AND w.createdBy = :resource ");
         
         if(filter.getTeam() != null)
            queryBuilder.append(" AND t.id = " + filter.getTeam());
         
         if(filter.getApplications() != null && !filter.getApplications().isEmpty())
            queryBuilder.append(" AND w.application.id in ( " + 
                  filter.getApplications().stream().map(String::valueOf).collect(Collectors.joining(",")) + ")");

         TypedQuery<WorkItem> query = entityManager.createQuery(queryBuilder.toString(), WorkItem.class);
         query.setParameter("resource", ResourcesHelper.getResource());
         returnList = query.getResultList();
      }
      else {
         returnList = workItemService.getWorkItemsCreatedByMeAndAssigendToMyTeams(ResourcesHelper.getResource(), teamsService.getByMember(ResourcesHelper.getResource()));
      }
      
      return checkSearchQuery(returnList, filter);
   }
   
   private void fillInStatuses(StringBuilder queryBuilder, TypeOfWorkItemView type) {
      if (type.getStatuses() != null && !type.getStatuses().isEmpty() && type.getStatuses().stream().anyMatch(Objects::nonNull)) {
         queryBuilder.append(" AND w.status.id IN ( ");
         for (ItemStatusView status : type.getStatuses()) {
            if (status != null && status.getName() != null && status.getId() != null)
               queryBuilder.append(status.getId() + ",");
         }
         queryBuilder.setLength(queryBuilder.length() - 1);
         queryBuilder.append("))");
      } else {
         queryBuilder.append(")");
      }
   }
   
   public List<WorkItem> checkSearchQuery(List<WorkItem> returnList, WorkItemFilterView filter) {
      List<WorkItem> workitems = new ArrayList<>();
      
      if (returnList.isEmpty() && filter.getSearchQuery() != null && !filter.getSearchQuery().isEmpty())
         workitems = fuzzySearch(filter.getSearchQuery());
      else if (!returnList.isEmpty() && filter.getSearchQuery() != null && !filter.getSearchQuery().isEmpty()) {
         List<WorkItem> tempList = new ArrayList<>();
         for (WorkItem workItem : returnList) {
            if (StringUtils.containsIgnoreCase(workItem.getName(), filter.getSearchQuery())
                  || StringUtils.containsIgnoreCase(workItem.getDescription(), filter.getSearchQuery())
                  || StringUtils.containsIgnoreCase(workItem.getNumber(), filter.getSearchQuery()))
               tempList.add(workItem);
         }
         workitems = tempList;
      }
      else {
         workitems = returnList;
      }

      Team activeTeam = contextHelper.getActiveTeam();
      if (activeTeam != null) {
         workitems =  workitems.stream().filter(x -> x.getTeam().getId().equals(activeTeam.getId())).collect(Collectors.toList());
      }
      
      return workitems;
   }

   public WorkItemFilterView getFilterFromCookie(String cookieName) {
      WorkItemFilterView filter = null;
      
      /*
       * Get cookie data from database
       */
      Cookie cookie = cookiesHelper.get(cookieName, ResourcesHelper.getResource());
      if (cookie == null) {
         /*
          * If cookie not exists, create cookie with default values (none)
          */
         if (filter == null) {
            filter = new WorkItemFilterView();
            
            cookiesHelper.save(cookieName, ResourcesHelper.getResource(), filter);
         }
      }
      else {
         /*
          * If cookie exists, get data from cookie
          */
         ObjectMapper mapper = new ObjectMapper();
         
         try {
            filter = mapper.readValue(cookie.getData(), WorkItemFilterView.class);
         }
         catch (Exception ex) {
            logger.error("Exception was caught when trying to serialize workitems filter.", ex);
         }
      }
      
      return filter;
   }
}
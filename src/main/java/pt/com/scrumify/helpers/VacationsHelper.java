package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.TypeOfWork;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.VacationService;

import java.util.List;

@Service
public class VacationsHelper {
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private VacationService vacationsService;
   @Autowired
   private TimesheetDayHelper timesheetDayHelper;
   @Autowired
   private TimesheetReviewHelper timesheetReviewHelper;

   @Transactional
   public void add(List<Vacation> vacations) {
      for (Vacation vacation : vacations) {
         /*
          * Get timesheet
          */
         Timesheet timesheet = timesheetService.getSheetByFortnight(vacation.getResource(), vacation.getDay().getYear().getId(), vacation.getDay().getMonth(), vacation.getDay().getFortnight());
         vacation.setTimesheet(timesheet);
         
         /*
          * Save absence
          */
         vacationsService.save(vacation);

         /*
          * Add hours to timesheet day
          */
         TimesheetDay day = timesheetDayHelper.addVacation(vacation);
         
         /*
          * Add hours to timesheet review
          */
         /*
          * TODO: convert to constant
          */
         timesheetReviewHelper.add(TypeOfWork.OTHER, day.getHours(), vacation.getDay().getYear(), vacation.getDay().getMonth(), vacation.getDay().getFortnight(), null, "99-01", "Vacations");
      }
   }

//   @Transactional
//   public void approve(Timesheet timesheet) {
//      List<Vacation> vacations = vacationsService.getByTimesheet(timesheet.getResource(), timesheet.getStartingDay().getYear(), timesheet.getStartingDay().getMonth(), timesheet.getStartingDay().getFortnight());
//      
//      for(Vacation vacation : vacations)
//         vacation.setTimesheet(timesheet);
//      
//      vacationsService.save(vacations);
//   }
   
   @Transactional
   public void remove(Integer id) {
      Vacation vacation = vacationsService.getById(id);
      
      /*
       * Decrease hours on timesheet day
       */
      TimesheetDay day = timesheetDayHelper.removeVacation(vacation);

      /*
       * Decrease hours on timesheet review
       */
      /*
       * TODO: convert to constant
       */
      timesheetReviewHelper.remove(TypeOfWork.OTHER, vacation.getDay().getYear(), vacation.getDay().getMonth(), vacation.getDay().getFortnight(), "99-01", "Vacations", day.getHours());
      
      /*
       * Delete vacation
       */
      vacationsService.delete(vacation);
   }
}
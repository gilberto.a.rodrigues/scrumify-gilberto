package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
public class TimesheetApprovalMonthlyPK implements Serializable {
   private static final long serialVersionUID = -3599940924307812581L;

   @Getter
   @Setter
   @Column(name = "year", nullable = false)
   private Integer year;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract", nullable = false)
   private Contract contract;
}
package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TEAM_MEMBERS)
@AssociationOverrides({ 
   @AssociationOverride(name = "pk.team", joinColumns = @JoinColumn(name = "team")),
   @AssociationOverride(name = "pk.resource", joinColumns = @JoinColumn(name = "resource")) 
})
public class TeamMember implements Serializable {
   private static final long serialVersionUID = -4356078386440856247L;

   @Getter
   @Setter
   @EmbeddedId
   private TeamMemberPK pk = new TeamMemberPK();

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY, targetEntity = Occupation.class)
   @JoinColumn(name = "occupation", nullable = true)
   private Occupation occupation = new Occupation();

   @Getter
   @Setter
   @Column(name = "approver")
   private boolean approver = false;

   @Getter
   @Setter
   @Column(name = "reviewer")
   private boolean reviewer = false;

   @Getter
   @Setter
   @Column(name = "allocation")
   private Integer allocation = 100;
   
   public TeamMember(TeamMemberPK pk) {
      super();
      
      this.pk = pk;
   }
      
   @Transient
   public Team getTeam() {
      return this.pk.getTeam();
   }

   public void setTeam(Team team) {
      this.pk.setTeam(team);
   }

   @Transient
   public Resource getResource() {
      return this.pk.getResource();
   }

   public void setResource(Resource resource) {
      this.pk.setResource(resource);
   }
}
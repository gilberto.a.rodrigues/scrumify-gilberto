package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_AVATARS)
public class Avatar implements Serializable {
   private static final long serialVersionUID = -4961952907791903286L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "gender", nullable = false)
   private Gender gender = new Gender(Gender.MALE);

   @Getter
   @Setter
   @Column(name = "image", length = 50, nullable = false)
   private String image;

   @Getter
   @Setter
   @OneToMany(mappedBy = "avatar")
   private List<Resource> resources;

   public Avatar(int id) {
      super();

      this.id = id;
   }
}
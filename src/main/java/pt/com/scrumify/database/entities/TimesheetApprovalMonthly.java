package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_TIMESHEETS_APPROVAL_MONTHLY)
@AssociationOverrides({ 
   @AssociationOverride(name = "pk.year", joinColumns = @JoinColumn(name = "year")),
   @AssociationOverride(name = "pk.contract", joinColumns = @JoinColumn(name = "contract"))
}) 
public class TimesheetApprovalMonthly implements Serializable {
   private static final long serialVersionUID = -3894611855608301952L;

   @Getter
   @Setter
   @EmbeddedId
   private TimesheetApprovalMonthlyPK pk = new TimesheetApprovalMonthlyPK();
   
   @Getter
   @Setter
   @Column(name = "january", nullable = false)
   private double january;
   
   @Getter
   @Setter
   @Column(name = "february", nullable = false)
   private double february;
   
   @Getter
   @Setter
   @Column(name = "march", nullable = false)
   private double march;
   
   @Getter
   @Setter
   @Column(name = "april", nullable = false)
   private double april;
   
   @Getter
   @Setter
   @Column(name = "may", nullable = false)
   private double may;
   
   @Getter
   @Setter
   @Column(name = "june", nullable = false)
   private double june;
   
   @Getter
   @Setter
   @Column(name = "july", nullable = false)
   private double july;
   
   @Getter
   @Setter
   @Column(name = "august", nullable = false)
   private double august;
   
   @Getter
   @Setter
   @Column(name = "september", nullable = false)
   private double september;
   
   @Getter
   @Setter
   @Column(name = "october", nullable = false)
   private double october;
   
   @Getter
   @Setter
   @Column(name = "november", nullable = false)
   private double november;
   
   @Getter
   @Setter
   @Column(name = "december", nullable = false)
   private double december;
   
   @Transient
   public double getTotal() {
      return this.january + this.february + this.march + this.april + this.may + this.june + this.july + this.august + this.september + this.october + this.november + this.december;
   }
   
   @Transient
   public double getBalance() {
      return this.getActual() + this.getPrevious() - this.getTotal();
   }
   
   @Transient
   public int getPrevious() {
      int result = 0;
      
      if (this.pk.getContract().getYear().getId() + 1 == this.pk.getYear()) {
         result = this.pk.getContract().getHours();
      }
         
      return result;
   }
   
   @Transient
   public int getActual() {
      int result = 0;
      
      if (this.pk.getContract().getYear().getId().equals(this.pk.getYear())) {
         result = this.pk.getContract().getHours1stYear();
      }
      else if (this.pk.getContract().getYear().getId() + 1 == this.pk.getYear()) {
         result = this.pk.getContract().getHours2ndYear();
      }
         
      return result;
   }
   
   public TimesheetApprovalMonthly(int year, Contract contract, double january, double february, double march, double april, double may, double june, double july, double august, double september, double october, double november, double december) {
      super();

      this.pk.setYear(year);
      this.pk.setContract(contract);
      this.setJanuary(january);
      this.setFebruary(february);
      this.setMarch(march);
      this.setApril(april);
      this.setMay(may);
      this.setJune(june);
      this.setJuly(july);
      this.setAugust(august);
      this.setSeptember(september);
      this.setOctober(october);
      this.setNovember(november);
      this.setDecember(december);
   }
}
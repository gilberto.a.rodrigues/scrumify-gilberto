package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
public class ContractPropertyPK implements Serializable {
   private static final long serialVersionUID = -3039700435313032926L;
   
   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Contract contract;
   
   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Property property;

   public ContractPropertyPK(Contract contract, Property property) {
      super();
      
      this.contract = contract;
      this.property = property;
   }
}
package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_CONTRACT_STATUS)
public class ContractStatus implements Serializable {
   private static final long serialVersionUID = -4320354829305622097L;

   public static final int PENDING = 1;
   public static final int OPEN = 2;
   public static final int SUSPENDED = 3;
   public static final int CLOSED = 4;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "status")
   private List<Contract> contracts = new ArrayList<>(0);
}
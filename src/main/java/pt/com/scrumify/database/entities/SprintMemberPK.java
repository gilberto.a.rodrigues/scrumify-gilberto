package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
public class SprintMemberPK implements Serializable {
   private static final long serialVersionUID = -5849873975350440811L;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Sprint sprint;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Resource resource;

   public SprintMemberPK(Sprint sprint, Resource resource) {
      super();
      
      this.sprint = sprint;
      this.resource = resource;
   }
}
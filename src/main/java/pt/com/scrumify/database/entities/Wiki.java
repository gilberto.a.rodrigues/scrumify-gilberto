package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Indexed
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_WIKIS)
public class Wiki implements Serializable {
   private static final long serialVersionUID = -4362227786778326388L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Field
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = true)
   private Application application;

   @Getter
   @Setter
   @Column(name = "active", nullable = false)
   private Boolean active = false;

   @Getter
   @Setter
   @IndexedEmbedded
   @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "wiki")
   private List<WikiPage> pages;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
   
   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
}
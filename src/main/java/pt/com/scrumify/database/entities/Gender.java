package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_GENDERS)
public class Gender implements Serializable {
   private static final long serialVersionUID = 5624838100631969402L;

   public static final Integer MALE = 1;
   public static final Integer FEMALE = 2;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   public Gender(Integer id) {
      super();

      this.id = id;
   }
}
package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
public class TimeSchedulePK implements Serializable {
   private static final long serialVersionUID = -1264458940163101084L;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Time time;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Schedule schedule;
}
package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_USERSTORY_NOTES) 
public class UserStoryNote implements Serializable {
   private static final long serialVersionUID = 2156685039738113947L;

   public static final int ORIGIN = 11;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "content", columnDefinition="CLOB", nullable = false)
   private String content;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "typeofnote", nullable = false)
   private TypeOfNote typeOfNote;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "userstory", nullable = false)
   private UserStory userStory;

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   public UserStoryNote(int typeOfNote) {
      super();
      
      this.id = 0;
      this.setTypeOfNote(new TypeOfNote(typeOfNote));
   }

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
   }
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.TypeOfAbsence;

import java.util.List;


public interface TypeOfAbsenceRepository extends JpaRepository<TypeOfAbsence, Integer> {
   TypeOfAbsence findByCodeAndName(String code, String name);
   List<TypeOfAbsence> findAllByOrderByNameAsc();
}
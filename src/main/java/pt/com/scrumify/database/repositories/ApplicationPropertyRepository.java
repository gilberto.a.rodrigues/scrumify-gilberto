package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.ApplicationProperty;

import java.util.List;


public interface ApplicationPropertyRepository extends JpaRepository<ApplicationProperty, Integer> {
   ApplicationProperty findByPkPropertyIdAndPkApplicationIdAndPkEnvironmentId(int property, int application, int environment);
   List<ApplicationProperty> findByPkApplicationIdAndPkEnvironmentId(int application, int environment);
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.ApplicationDataSource;

import java.util.List;


public interface ApplicationDataSourceRepository extends JpaRepository<ApplicationDataSource, Integer> {
   ApplicationDataSource findByIdAndApplicationIdAndEnvironmentId(int datasource, int application, int environment);   
   List<ApplicationDataSource> findByApplicationIdAndEnvironmentId(int application, int environment);   
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.ApplicationLink;

import java.util.List;

public interface ApplicationLinkRepository extends JpaRepository<ApplicationLink, Integer> {
   ApplicationLink findByIdAndApplicationIdAndEnvironmentId(int link, int application, int environment);
   List<ApplicationLink> findByApplicationIdAndEnvironmentId(int application, int environment);   
}
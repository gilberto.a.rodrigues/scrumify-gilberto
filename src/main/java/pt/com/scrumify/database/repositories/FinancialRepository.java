package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Financial;

import java.util.List;


public interface FinancialRepository extends JpaRepository<Financial, Integer> {
   List<Financial> getByContractAndType(Contract contract, String type);
}
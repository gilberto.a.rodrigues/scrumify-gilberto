package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Avatar;
import pt.com.scrumify.database.entities.Gender;

import java.util.List;

public interface AvatarRepository extends JpaRepository<Avatar, Integer> {
	List<Avatar> getByGender(Gender gender);
	
	Avatar findTop1ByGender(Gender gender);
	Avatar findByIdAndGender(Integer avatar, Gender gender);
}
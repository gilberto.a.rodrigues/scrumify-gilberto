package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

public interface ApplicationRepository extends JpaRepository<Application, Integer> {
   
   public List<Application> findAllByOrderByNameAsc();
   List<Application> findDistinctBySubAreaTeamsMembersPkResourceOrderByNameAsc(Resource resource);
   
}
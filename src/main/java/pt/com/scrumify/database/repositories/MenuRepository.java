package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu, Integer> {
   @Query(nativeQuery = false, 
          value = "SELECT m " + 
                  "FROM Menu m " + 
                  "INNER JOIN m.roles r " + 
                  "INNER JOIN r.profiles p " + 
                  "INNER JOIN p.resources u " + 
                  "WHERE u.active = TRUE " + 
                  "AND u = :resource " + 
                  "ORDER BY m.order ASC")
   public List<Menu> getByResource(@Param("resource") Resource resource);
}
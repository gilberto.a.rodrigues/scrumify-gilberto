package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.UserStoryHistory;

import java.util.List;

public interface UserStoryHistoryRepository extends JpaRepository<UserStoryHistory, Integer> {
   
   List<UserStoryHistory> findByUserStoryIdOrderByLastUpdateDesc(Integer iduserStory);
}
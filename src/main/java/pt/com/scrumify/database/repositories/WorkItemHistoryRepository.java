package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.WorkItemHistory;

import java.util.List;

public interface WorkItemHistoryRepository extends JpaRepository<WorkItemHistory, Integer> {
   
   List<WorkItemHistory> findByWorkItemIdOrderByLastUpdateDesc(Integer idworkitem);
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pt.com.scrumify.database.entities.TimesheetExport;
import pt.com.scrumify.database.entities.TimesheetExportPK;

import java.util.List;

public interface TimesheetExportRepository extends JpaRepository<TimesheetExport, TimesheetExportPK> {
   @Query("SELECT te " +
          "FROM TimesheetExport te " +
          "WHERE te.pk.year = :year " +
          "AND te.pk.month = :month " +
          "AND te.pk.resource IN (:resources)")
  List<TimesheetExport> findByResourcesYearAndMonth(@Param("resources") List<String> resources, @Param("year") Integer year, @Param("month") Integer month);
   
   @Query("SELECT te " +
          "FROM TimesheetExport te " +
          "WHERE te.pk.year = :year " +
          "AND te.pk.month = :month " +
          "AND te.pk.fortnight = :fortnight " +
          "AND te.pk.resource IN (:resources)")
   List<TimesheetExport> findByResourcesYearMonthAndFortnight(@Param("resources") List<String> resources, @Param("year") Integer year, @Param("month") Integer month, @Param("fortnight") Integer fortnight);
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.TypeOfWorkItem;

import java.util.List;

public interface TypeOfNoteRepository extends JpaRepository<TypeOfNote, Integer> {   

   
   @Query(nativeQuery = false,
         value = "SELECT type " + 
                 "FROM TypeOfNote type " +
                 "INNER JOIN type.typesOfWorkItem twi " +
                 "WHERE twi = :typeOfWorkItem")
  List<TypeOfNote> getByTypeOfWorkItem(@Param("typeOfWorkItem") TypeOfWorkItem typeOfWorkItem);
}
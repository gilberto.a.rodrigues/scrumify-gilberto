package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamContract;
import pt.com.scrumify.database.entities.TeamContractPK;

import java.util.List;

public interface TeamContractRepository extends JpaRepository<TeamContract, TeamContractPK> {
   @Query(nativeQuery = false,
          value = "SELECT tm " +
                  "FROM TeamContract tm " +
                  "WHERE tm.pk.team = :team ")
   List<TeamContract> getByTeam(@Param("team") Team team);
   
}
package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Workflow;

public interface WorkflowRepository extends JpaRepository<Workflow, Integer> {
   
}
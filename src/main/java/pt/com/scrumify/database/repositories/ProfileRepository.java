package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Profile;

import java.util.List;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {
   List<Profile> getBySystem(boolean system);
   List<Profile> findBySystemFalse();
}
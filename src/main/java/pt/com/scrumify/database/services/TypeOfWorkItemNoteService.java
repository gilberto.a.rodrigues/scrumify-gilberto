package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.TypeOfWorkItem;

import java.util.List;

public interface TypeOfWorkItemNoteService {
   TypeOfNote getById(Integer id);
   TypeOfNote save(TypeOfNote typeOfWorkItemNote);
   List<TypeOfNote> getAll();
   List<TypeOfNote> getByTypeOfWorkItem(TypeOfWorkItem typeOfWorkItem);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Activity;

import java.util.List;

public interface ActivityService {
   Activity getOne(Integer id);
   List<Activity> getAll();
   List<Activity> getByTechnicalAssistance(boolean technicalAssistance);
   Activity save (Activity activity);
}
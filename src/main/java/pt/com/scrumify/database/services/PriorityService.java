package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Priority;

import java.util.List;

public interface PriorityService {
   Priority getOne(Integer id);
   List<Priority> getAll();
}
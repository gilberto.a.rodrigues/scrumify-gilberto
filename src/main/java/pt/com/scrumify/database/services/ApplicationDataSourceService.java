package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ApplicationDataSource;

import java.util.List;

public interface ApplicationDataSourceService {
   void delete(ApplicationDataSource datasource);
   List<ApplicationDataSource> getByApplicationAndEnvironment(int application, int environment);
   ApplicationDataSource getOne(int datasource, int application, int environment);
   ApplicationDataSource save(ApplicationDataSource datasource);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ApplicationProperty;

import java.util.List;

public interface ApplicationPropertyService {
   void delete(ApplicationProperty property);
   List<ApplicationProperty> getByApplicationAndEnvironment(int application, int environment);
   ApplicationProperty getOne(int property, int application, int environment);
   ApplicationProperty save (ApplicationProperty property);
}
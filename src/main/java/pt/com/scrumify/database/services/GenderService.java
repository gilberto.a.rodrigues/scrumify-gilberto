package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Gender;

import java.util.List;

public interface GenderService {
   Gender getOne(Integer id);
   List<Gender> getAll();
}
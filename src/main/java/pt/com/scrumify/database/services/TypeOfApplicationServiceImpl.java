package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.TypeOfApplication;
import pt.com.scrumify.database.repositories.TypeOfApplicationRepository;

import java.util.List;

@Service
public class TypeOfApplicationServiceImpl implements TypeOfApplicationService {
   
   @Autowired
   private TypeOfApplicationRepository repository;

   @Override
   public TypeOfApplication getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public TypeOfApplication save(TypeOfApplication type) {
      return repository.save(type);
   }

   @Override
   public List<TypeOfApplication> listAll() {
      return repository.findAll(Sort.by("name"));
   }
}
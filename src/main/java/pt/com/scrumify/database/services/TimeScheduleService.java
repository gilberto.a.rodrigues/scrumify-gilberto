package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Schedule;
import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.TimeSchedule;
import pt.com.scrumify.database.entities.Year;

import java.util.List;

public interface TimeScheduleService {
   List<TimeSchedule> getByYearMonthAndFortnight(Year year, Integer month, Integer fortnight, Schedule schedule);
   TimeSchedule getByDay(Time day, Schedule schedule);
}
package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.TimesheetApprovalMonthly;
import pt.com.scrumify.database.repositories.TimesheetApprovalMonthlyRepository;

import java.util.List;

@Service
public class TimesheetApprovalMonthlyServiceImpl implements TimesheetApprovalMonthlyService {
   @Autowired
   private TimesheetApprovalMonthlyRepository repository;

   @Override
   public List<TimesheetApprovalMonthly> findByContractsAndYear(List<Contract> contracts, int year) {
      return this.repository.findByContractsAndYear(contracts, year);
   }
}
package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.ApplicationStatus;
import pt.com.scrumify.database.repositories.ApplicationStatusRepository;

import java.util.List;

@Service
public class ApplicationStatusServiceImpl implements ApplicationStatusService {
   
   @Autowired
   private ApplicationStatusRepository repository;

   @Override
   public ApplicationStatus getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public ApplicationStatus save(ApplicationStatus status) {
      return repository.save(status);
   }

   @Override
   public List<ApplicationStatus> listAll() {
      return repository.findAll();
   }
}
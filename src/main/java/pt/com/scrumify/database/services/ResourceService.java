package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

import java.util.List;

public interface ResourceService {
	Resource getOne(Integer id);
	Resource save(Resource resource);
   Resource getByEmail(String email);
   Resource getByUsername(String username);
   Resource getByUsernameAndActive(String username, boolean active);
   List<Resource> getNonSystem();
   List<Resource> getByApproved(boolean approved);
   List<Resource> getNonSystemUsersOrderByProfileAndResourceName();
   List<Resource> getByTeam(Team team);
   List<Resource> getByTeams(List<Team> team);
   List<Resource> getByTeamExceptCurrentResource(Team team, Resource resource);
   List<Resource> getByExecutiveProfile();
   List<Resource> getApproversBySubArea(SubArea subArea);
   List<Resource> getBySubArea(SubArea subArea);
   List<Resource> getResourcesOfTimesheetSubmit(List<Resource> resource, Integer year, Integer month, Integer day);
   List<Resource> getTop3ResourcesWithTimesheetUnapproved(List<Resource> resource, Integer year, Integer month, Integer day);
   List<Resource> getResourcesWithTimesheetUnreviewed(List<Resource> resources);
   List<Resource> getResourcesWithTimesheetUnapproved(List<Resource> resource, Integer year, Integer month, Integer day);
   List<Resource> getResourcesWithTimesheetApprovedByContract(List<Team> teams, Integer year, Integer month, Integer fortnight, Contract contract);
   List<Resource> getByTeamsAndContract(List<Team> teams, Contract contract);
   
   List<Resource> getMembersOfSameTeam(Resource resource);
   List<Resource> getMembersOfSameTeamAndContract(Contract contract);
   List<Resource> findWithTimesheetsToReview(List<Team> teams);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

public interface MenuService {
   Menu getOne(Integer id);
   List<Menu> getByResource(Resource resource);
   Menu save(Menu area);
}
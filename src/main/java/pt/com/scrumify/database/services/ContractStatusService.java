package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ContractStatus;

import java.util.List;


public interface ContractStatusService {
   List<ContractStatus> getAllStatus();
   List<ContractStatus> getByIds(int[] ids);

}
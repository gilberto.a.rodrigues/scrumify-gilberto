package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

import java.util.List;

public interface SubAreaService {
   SubArea getOne(Integer id);
   SubArea save(SubArea subArea);
   List<SubArea> getAll();
   List<SubArea> byTeams(List<Team> teams);
   List<SubArea> getByAreaAndResource(Area area, Resource resource);
   List<SubArea> listSubarea(SubArea subArea);
   List<SubArea> listSubareasByResource(Resource resource);
}
   
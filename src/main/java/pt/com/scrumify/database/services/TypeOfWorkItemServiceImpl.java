package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.TypeOfWorkItem;
import pt.com.scrumify.database.repositories.TypeOfWorkItemRepository;
import pt.com.scrumify.database.repositories.WorkItemStatusRepository;

import java.util.List;

@Service
public class TypeOfWorkItemServiceImpl implements TypeOfWorkItemService {

   @Autowired
   private TypeOfWorkItemRepository repository;
   
   @Autowired
   private WorkItemStatusRepository statusRepository;

   @Override
   public List<TypeOfWorkItem> getAll() {
      return repository.findAll();
   }

   @Override
   public TypeOfWorkItem getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public List<TypeOfWorkItem> getAvailable() {
      List<TypeOfWorkItem> typesAvailable = repository.findByWorkItemIsTrueOrderByNameAsc();
      
      for(TypeOfWorkItem type : typesAvailable) {
         type.setStatuses(statusRepository.findByTypeOfWorkItemById(type.getId()));
      }
      
      return typesAvailable;
   }
}
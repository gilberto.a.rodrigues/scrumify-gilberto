package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;

import java.util.List;

public interface WikiService {
   Wiki getOne(Integer id);
   Wiki save(Wiki wiki);
   List<Wiki> getAll();
   List<Wiki> getByResource(Resource resource);
   void delete(Wiki wiki);
}
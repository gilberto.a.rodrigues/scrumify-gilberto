package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ApplicationLink;

import java.util.List;

public interface ApplicationLinkService {
   void delete(ApplicationLink link);
   List<ApplicationLink> getByApplicationAndEnvironment(int application, int environment);
   ApplicationLink getOne(int link, int application, int environment);
   ApplicationLink save(ApplicationLink link);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfWork;

import java.util.List;

public interface TypeOfWorkService {
   TypeOfWork getById(Integer id);
   List<TypeOfWork> getAll();
   TypeOfWork save(TypeOfWork typeOfWork);
}
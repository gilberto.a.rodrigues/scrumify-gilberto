package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamContract;
import pt.com.scrumify.database.entities.TeamContractPK;
import pt.com.scrumify.database.repositories.TeamContractRepository;

import java.util.List;


@Service
public class TeamContractServiceImpl implements TeamContractService {
   @Autowired
   private TeamContractRepository repository;

   @Override
   public void delete(TeamContract teamContract) {
      this.repository.delete(teamContract);
   }

   @Override
   public TeamContract getOne(TeamContractPK pk) {
      return this.repository.getOne(pk);
   }

   @Override
   public List<TeamContract> getByTeam(Team team) {
      return this.repository.getByTeam(team);
   }
   
   @Override
   public TeamContract save(TeamContract teamContract) {
      return this.repository.save(teamContract);
   }
}
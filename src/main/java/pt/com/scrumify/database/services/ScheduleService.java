package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Schedule;

import java.util.Date;
import java.util.List;

public interface ScheduleService {
   Schedule getOne(Integer id);
   Schedule save(Schedule schedule);
	
   List<Schedule> getAll();
   Schedule getActiveSchedule(Resource resource, Date date);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Release;

import java.util.List;

public interface ReleaseService {
   Release getOne(int id);
   List<Release> getByApplication(Integer application);
   void delete(Release release);
   void deleteById(Integer id);
   void save(Release release);   
}
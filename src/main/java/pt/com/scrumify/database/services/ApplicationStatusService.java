package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ApplicationStatus;

import java.util.List;

public interface ApplicationStatusService {
   ApplicationStatus getOne(Integer id);
   ApplicationStatus save(ApplicationStatus status);
   List<ApplicationStatus> listAll();
}
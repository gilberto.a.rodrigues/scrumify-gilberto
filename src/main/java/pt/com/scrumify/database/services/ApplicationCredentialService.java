package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ApplicationCredential;

import java.util.List;

public interface ApplicationCredentialService {
   void delete(ApplicationCredential credencial);
   List<ApplicationCredential> getByApplicationAndEnvironment(int application, int environment);
   ApplicationCredential getOne(int credential, int application, int environment);
   ApplicationCredential save(ApplicationCredential credential);
}
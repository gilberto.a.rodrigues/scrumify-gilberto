package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.WikiPage;

import java.util.List;

public interface WikiPageService {
   WikiPage getOne(Integer id);
   WikiPage save(WikiPage page);
   List<WikiPage> getAll();
   void delete(WikiPage page);
}
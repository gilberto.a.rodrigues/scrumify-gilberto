package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfAbsence;

import java.util.List;

public interface TypeOfAbsenceService {
   List<TypeOfAbsence> getAllTypes();
   TypeOfAbsence findByCodeAndName(String code, String name);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Environment;

import java.util.List;

public interface EnvironmentService {
   List<Environment> getAll();
   Environment getOne(int id);
   Environment getByName(String environmentName);
   void save(Environment environment);
}
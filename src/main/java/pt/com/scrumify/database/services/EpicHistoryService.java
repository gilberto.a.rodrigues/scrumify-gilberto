package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.EpicHistory;

import java.util.List;

public interface EpicHistoryService {
	
   EpicHistory  save(EpicHistory epicHistory);
   List<EpicHistory> getEpicHistorybyEpic(Integer idepic);
  
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.entities.WorkflowTransition;

import java.util.List;

public interface WorkflowService {
   Workflow getOne(Integer id);
   Workflow save(Workflow workFlow);
   List<Workflow> getAll();
   
   WorkflowTransition saveTransition(WorkflowTransition transition);
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.entities.TimesheetApprovalView;

import java.util.List;

public interface TimesheetApprovalService {
   TimesheetApproval getOne(Integer id);
   TimesheetApproval getOne(Resource resource, Year year, Integer month, Integer fortnight, String code, String description);
   TimesheetApproval save(TimesheetApproval timesheetsBillable); 
   List<TimesheetApproval> getAll();
   TimesheetApproval getbyContract(Contract contract);
   List<TimesheetApproval> getbyYearAndArea(Year year, Area area);
   List<TimesheetApproval> getbyYearAndMonthAndArea(Year year, Integer month ,Area area);
   TimesheetApproval getbyYearAndMonthAndResourceAndFortnightAndContract(Year year, Integer month, Resource resource, Integer fortnight, Contract contract);
   List<TimesheetApproval> getbyYearAndMonthAndFortnightAndTeams(Integer year, Integer month, Integer fortnight, List<Team> teams);
   List<TimesheetApproval> getby(Integer year, Integer month , Integer fortnight, Integer resource);
   List<TimesheetApproval> getbyYearAndMonthAndFortnightAndContract(Year year, Integer month, Integer fortnight, Integer contract);
   List<TimesheetApproval> getbyYearAndMonthAndContract(Year year, Integer month, Integer contract);
   List<TimesheetApproval> getbyTimesheetAndResource(Integer year, Integer month, Integer fortnight, Integer resource);
   List<TimesheetApproval> saveAll(List<TimesheetApproval> billables);
   long getHoursByYearAndMonthAndAreaAndFortnight(Year year, Integer month, Contract contract, Integer fortnight);
   List<TimesheetApprovalView> getbyYearAndAreaAndMonth(Year year, Integer month, Area area);
   Integer getHoursForStatusReport(Contract contract, Year year, Integer month);
   Integer getHoursForStatusReportByContract(Contract contract);
   List<TimesheetApprovalView> getbyYearAndMonth(Year year, Integer month);
   List<TimesheetApproval> getContractsInfo(Year year);
   void delete(List<TimesheetApproval> approvals);
   void delete(Integer id);
}
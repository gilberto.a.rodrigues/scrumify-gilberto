package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.*;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.entities.ResourceReport;
import pt.com.scrumify.helpers.ResourcesHelper;

import java.util.*;
import java.util.Map.Entry;

@Service
public class ReportServiceImpl implements ReportService {
   
   @Autowired
   private WorkItemService workitemsService;
   @Autowired
   private TeamService teamsService;

   @Override
   public Map<Resource, List<ResourceReport>> reportByResource(List<Timesheet> timesheets){
      Map<Resource, List<ResourceReport>> resourceReport = new HashMap<>();
      
      for(Timesheet timesheet : timesheets){
         if(resourceReport.containsKey(timesheet.getResource())){
            resourceReport = monthlyReport(timesheet,resourceReport);
         }else{ 
            List<ResourceReport> lista = new ArrayList<>();
            ResourceReport report = report(timesheet);
            lista.add(report);
            resourceReport.put(timesheet.getResource(), lista);
         }  
      } 
      Iterator<Entry<Resource, List<ResourceReport>>> it = resourceReport.entrySet().iterator();
      while (it.hasNext()) {
         Entry<Resource, List<ResourceReport>> pair = it.next();
         Collections.sort((List<ResourceReport>)pair.getValue(), new SortbyMonth()); 
      }
     return resourceReport; 
   }

   @Override
   public ResourceReport report(Timesheet timesheet){
   ResourceReport report = new ResourceReport();
   report.setMonth(timesheet.getStartingDay().getMonth());
   report.setAbsence(timesheet.getDays().stream().mapToInt(t -> t.getHoursVacation().intValue()).sum() + timesheet.getDays().stream().mapToInt(t -> t.getHoursCompensationDay().intValue()).sum() + timesheet.getDays().stream().mapToInt(t -> t.getHoursAbsence().intValue()).sum());
   report.setHours(timesheet.getDays().stream().mapToInt(t -> t.getHours().intValue()).sum());
   report.setHoursSpent(timesheet.getDays().stream().mapToInt(i -> i.getTimeSpent().intValue()).sum());
   return report;
   }
   
   @Override
   public ResourceReport reportByWorkItem(WorkItemWorkLog worklog){
      ResourceReport report = new ResourceReport();
      report.setMonth(worklog.getDay().getMonth());
      report.setHoursSpent(worklog.getTimeSpent());
      return report;
   }

   @Override
   public Map<Contract, List<ResourceReport>> reportByWorkItem(ReportView reportView) {
      Map<Contract, List<ResourceReport>> workitemReport = new HashMap<>();
      List<WorkItem> workitems = getWorkItems(reportView);
      for(WorkItem workitem : workitems){
         if(!workitemReport.containsKey(workitem.getContract())){ 
            List<ResourceReport> lista = new ArrayList<>();
            for(int i=1 ; i<=12 ; i++)
               lista.add(new ResourceReport(i));
            workitemReport.put(workitem.getContract(), lista);
         } 
         workitemReport = monthlyReportByWorkitem(workitem,workitemReport);
      }       
      return workitemReport;
   }
   
   @Override
   public Map<Team, List<ResourceReport>> reportByTeam(ReportView reportView) {
      Map<Team, List<ResourceReport>> teamReport = new HashMap<>();
      List<WorkItem> workitems = getWorkItems(reportView);
      for(WorkItem workitem : workitems){
         if(!teamReport.containsKey(workitem.getTeam())){
            List<ResourceReport> lista = new ArrayList<>();
            teamReport.put(workitem.getTeam(), lista);
         }
         teamReport = monthlyReportByTeam(workitem, teamReport);
      }

      return teamReport;
   }
   
   @Override
   public Map<Resource, List<ResourceReport>> monthlyReport(Timesheet timesheet, Map<Resource, List<ResourceReport>> resourceReport){
      List<ResourceReport> listareport = resourceReport.get(timesheet.getResource());
      if(listareport.stream().anyMatch(t -> t.getMonth() == timesheet.getStartingDay().getMonth())){
        for(ResourceReport report : listareport){
            if(report.getMonth() == timesheet.getStartingDay().getMonth()){             
            report.setHours(report.getHours() + timesheet.getDays().stream().mapToInt(t -> t.getHours().intValue()).sum());
            report.setHoursSpent(report.getHoursSpent() + timesheet.getDays().stream().mapToInt(i -> i.getTimeSpent().intValue()).sum());
            report.setAbsence(report.getAbsence() + timesheet.getDays().stream().mapToInt(t -> t.getHoursVacation().intValue()).sum() + timesheet.getDays().stream().mapToInt(t -> t.getHoursCompensationDay().intValue()).sum() + timesheet.getDays().stream().mapToInt(t -> t.getHoursAbsence().intValue()).sum());
            }
         }      
      }else{
         ResourceReport monthreport = report(timesheet);
         listareport.add(monthreport);
      }      
      return resourceReport;
   }
   
   class SortbyMonth implements Comparator<ResourceReport> 
   { 
       public int compare(ResourceReport a, ResourceReport b) 
       { 
           return a.getMonth()- b.getMonth(); 
       } 
   }
   
   @Override
   public Map<Contract, List<ResourceReport>> monthlyReportByWorkitem(WorkItem workitem, Map<Contract, List<ResourceReport>> workitemReport){
      List<ResourceReport> listareport = workitemReport.get(workitem.getContract());
      for(WorkItemWorkLog worklog : workitem.getWorkLogs()){
         if(listareport.stream().anyMatch(t -> t.getMonth() == worklog.getDay().getMonth())){
           for(ResourceReport report : listareport){
               if(report.getMonth() == worklog.getDay().getMonth()){             
                  report.setHoursSpent(report.getHoursSpent() + worklog.getTimeSpent());
               }
            }      
         }else{
            ResourceReport monthreport = reportByWorkItem(worklog);
            listareport.add(monthreport);
         }
      }
      return workitemReport;
   }
   
   @Override
   public Map<Team, List<ResourceReport>> monthlyReportByTeam(WorkItem workitem, Map<Team, List<ResourceReport>> teamReport){
      List<ResourceReport> listareport = teamReport.get(workitem.getTeam());
      for(WorkItemWorkLog worklog : workitem.getWorkLogs()){
         if(listareport.stream().anyMatch(t -> t.getMonth() == worklog.getDay().getMonth()) && listareport.stream().anyMatch(t -> t.getContract() == workitem.getContract())){
            for(ResourceReport report : listareport){
               if(report.getMonth() == worklog.getDay().getMonth() && report.getContract() == workitem.getContract()){             
                  report.setHoursSpent(report.getHoursSpent() + worklog.getTimeSpent());
               }
            }     
         }else{
            ResourceReport monthreport = reportByWorkItem(worklog);
            monthreport.setContract(workitem.getContract());
            listareport.add(monthreport);
         }
      }

      return teamReport;
   }
   
   @Override
   public List<WorkItem> getWorkItems(ReportView reportView){
      List<WorkItem> workitems;
      if(reportView.getResource() != null){
         workitems = workitemsService.getWorkItemsByTeamAndAreaAndResource(reportView.getTeam(), reportView.getYear(), reportView.getArea(), reportView.getResource());
      }else if(reportView.getTeam() != null){
         workitems = workitemsService.getWorkItemsByTeamAndArea(reportView.getTeam(), reportView.getYear(), reportView.getArea());
      }else if(reportView.getArea() != null){
         workitems = workitemsService.getWorkItemsByTeamsAndArea(teamsService.getByMember(ResourcesHelper.getResource()), reportView.getYear(), reportView.getArea());
      }else{
         workitems = workitemsService.getWorkItemsByTeams(teamsService.getByMember(ResourcesHelper.getResource()), reportView.getYear());
      }
      return workitems;
   }
}
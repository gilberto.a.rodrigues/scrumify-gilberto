package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.repositories.AreaRepository;

import java.util.List;

@Service
public class AreaServiceImpl implements AreaService {
   @Autowired
   private AreaRepository areaRepository;

   @Override
   public Area getOne(Integer id) {
      return this.areaRepository.getOne(id);
   }

   @Override
   public Area save(Area area) {
      return this.areaRepository.save(area);
   }

   @Override
   public List<Area> listAll() {
      return this.areaRepository.findAll();
   }
   
   @Override
   public List<Area> getByResource (Resource resource){
      return areaRepository.getByResource(resource);
   }
}
package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Profile;

import java.util.List;

public interface ProfileService {
   Profile getOne(Integer id);
   Profile save(Profile profile);
   List<Profile> getAll();
   List<Profile> getNonSystem();
   List<Profile> getBySystem(boolean system);
}
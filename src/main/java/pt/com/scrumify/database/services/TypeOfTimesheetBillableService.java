package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;

import java.util.List;

public interface TypeOfTimesheetBillableService {
   TypeOfTimesheetApproval getOne(Integer id);
   List<TypeOfTimesheetApproval> listAll();
}
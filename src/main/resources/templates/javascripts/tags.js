$(document).ready(function() {
  initialize();
});

function initialize() {
  $('.ui.dropdown').dropdown();

  formValidation();
}

function formSubmission() {   
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TAGS_SAVE}}]];
  var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TAGS}}]];	
	
  ajaxpost(url, $("#tags-form").serialize(), "", true, function() {
    initialize();
  }, returnurl);
}

function formValidation() {
  $('form').form({
    on : 'blur',
    inline : true,
    fields : {
      name : {
        identifier : 'name',
        rules : [ {
          type : 'empty',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      },
      grpup : {
        identifier : 'group',
        rules : [ {
          type : 'empty',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      },
      subarea : {
        identifier : 'subArea',
        rules : [ {
          type : 'empty',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      }
    },           	  
    onSuccess : function(event) {
      event.preventDefault();
      
      formSubmission();
    }
  });
}

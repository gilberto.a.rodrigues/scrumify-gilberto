$(document).ready(function() {
   initialize();	  
});
	        
function initialize() {
	validateDates();
		
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('select.dropdown').dropdown();
	formValidation();
}

function createVacation(){
	
	var url =  [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_CREATE}}]] ;
	   
	ajaxget(url,  "div[id='modal']", function() {
        initialize();
    }, true);
}



function deleteVacation(id) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	ajaxdelete(url, "div[id='vacations-list']", function() {
	      initialize();
    }, false);
	
}

function formSubmission() {   

	   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS_SAVE}}]];
	   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_VACATIONS}}]];
	   ajaxpost(url, $("#vacations-form").serialize(), "", false, function() {
		   initialize();
	   }, returnurl);
	}

function formValidation() {
	   $("#vacations-form").form({
	      on : 'blur',
	      inline : true,
	      fields : {
	    	  startDay : {
	            identifier : 'startDay',
	            rules : [ {
	               type : 'empty',
	               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	            } ]
	         }, 
	         timesheet : {
	             identifier : 'startDay',
	             rules : [ {
	   	            type : 'timesheet',
	   	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	   	            value: "input[id='input-startDay']"			            	
	         	}]
	         }, 
	         endDayTimesheet : {
	             identifier : 'endDay',
	             rules : [ {
	   	            type : 'timesheet',
	   	            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_TIMESHEET}}]],
	   	            value: "input[id='input-endDay']"			            	
	         	}]
	         } 
	         
	      },
	      onSuccess : function(event) {
	         event.preventDefault();
	         
	         formSubmission();
	      }
	   });
	}

function validateDates(){
	
	var localizedtext = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	var minDate = $("input[type=hidden][id='minDate']").val();
	var maxDate = $("input[type=hidden][id='maxDate']").val();
   
	calendar('startDay', 'date', localizedtext, minDate, maxDate, 'endDay');
    

}
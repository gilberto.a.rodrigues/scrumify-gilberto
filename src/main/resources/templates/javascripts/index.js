$(document).ready(function() {   
   getunapprovedresources();
   gettimesheetstoreview();
   getContracts();
   getuserteams();
   getWorkitems();
   
   initialize();
});


function initialize() {
	
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	$('.ui.relaxed.small.list').popup({
		position: 'top center'
    });
}


function getunapprovedresources() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_UNAPPROVED_AJAX}}]];
   
   ajaxget(url, "div[id='unapprovedresources']");
};

function gettimesheetstoreview() {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_TIMESHEET_UNREVIEWED_AJAX}}]];
	   
	ajaxget(url, "div[id='resourcesunreviewedtimesheets']");
};

function getuserteams() {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_USER_TEAMS_AJAX}}]];
	   
	ajaxget(url, "div[id='userteams']");
};

function getContracts() {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_CONTRACTS_LIST_AJAX}}]];

	ajaxget(url, "div[id='contracts']", function() {
	   $('.ui.progress').progress();
   });
};

function getWorkitems() {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WORKITEMS_LIST_AJAX}}]];
	   
	ajaxget(url, "div[id='workitems']", function() {
		$('.ui.dropdown').dropdown();
		$('select.dropdown').dropdown();
		$('.ui.mini').popup({
        position: 'top center'
		});
$('.ui.progress').progress();
   }); 
};

function assign(resource, workitem) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).WORKITEMS_API_MAPPING_UPDATE_ASSIGNED}}]] + workitem +'/' + resource;

	ajaxget(url, "div[id='workitems']", function() {
		getWorkitems();
   });
	
};
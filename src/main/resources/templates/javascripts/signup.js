$(document).ready(function() {

	ajaxtab(true, [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SIGNUP}}]], true, function(settings) {
      switch(settings.urlData.tab) {
         case "avatars":
            getavatars();
            break;
         case "personalinfo":
         default:
            getpersonalinfo();
            break;
      }
   });

   initialize();

});

function initialize() {
   
   $('.ui.radio.checkbox').checkbox();
   $('.ui.toggle.checkbox').checkbox();
   $('select.dropdown').dropdown();
   
   formValidation();
   ongenderchange();
}
        
function getpersonalinfo() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_SIGNUP_AJAX}}]];
   
   ajaxpost(url, $("#signup-form").serialize(), "div[data-tab='personalinfo']", false, function() {
      initialize();
   }); 
};

function getavatars() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_AVATARS_SIGNUP_AJAX}}]];
   
   ajaxpost(url, $("#signup-form").serialize(), "div[data-tab='avatars']", false, function() {
      $('.ui.radio.checkbox').checkbox();
           
      formValidation();
   });
};
  
function getgender() {
   var gender = $("input[type=radio][name='gender']:checked").val();
     
   if (gender == null)
      return 1;
   else
      return gender;
};

function ongenderchange() {
   $("input[type=radio][name='gender']").change(function() {
      $("input[type=radio][name='avatar']").prop('checked', false);
   });
}

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SIGNUP}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_INDEX}}]];
   ajaxpost(url, $("#signup-form").serialize(), "div[data-tab='personalinfo']", false, function() {
	   initialize();
   }, returnurl);
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         abbreviation : {
            identifier : 'abbreviation',
            rules : [ {
               type : 'exactLength[3]',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_LENGTH}(3)}]]
            } ]
         },
         name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         email : {
            identifier : 'email',
            rules : [ {
               type : 'email',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMAIL}}]]
            } ]
         },
         gender : {
            identifier : 'gender',
            rules : [ {
               type : 'checked',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_CHOICE}(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_GENDER}})}]]
            } ]
         },
         avatar : {
            identifier : 'avatar',
            rules : [ {
               type : 'checked',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_CHOICE}(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_AVATAR}})}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}
$(document).ready(function() {
   initialize();
});

function initialize() {
	
	$('.ui.toggle.checkbox').checkbox();
	formValidation();
}

function initializeSettings() {
	var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	calendar('startingDay', 'date', json);
	calendar('endingDay', 'date', json);
	
	settingformValidation();
}


function formSubmission() {   
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES_SAVE}}]];
	var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES}}]];
		
	ajaxpost(url, $("#schedule-form").serialize(), "", false, function() {
	   initialize();
	}, returnurl);
}

function formValidation() {
	   $("#schedule-form").form({
	      on : 'blur',
	      inline : true,
	      fields : {
	    	  name : {
	               identifier : 'name',
	               rules : [ {
	                  type : 'empty',
	                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	               } ]
	            }
	      },           	  
	      onSuccess : function(event) {
	          event.preventDefault(); 
	          formSubmission();
	      }
	   });
}

function settingformValidation() {
   $('#setting-form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  startingDay : {
  	        identifier : 'startingDay',
  	        rules : [ {
  	           type : 'empty',
  	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
  	        } ]
  	      }, 
  	      endingDay : {
  	        identifier : 'endingDay',
  	        rules : [ {
  	           type : 'empty',
  	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
  	        } ]
  	      }, 
    	  monday : {
            identifier : 'monday',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
	   	 tuesday : {
	        identifier : 'tuesday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     }, 
	     wednesday : {
	        identifier : 'wednesday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     },
	     thursday : {
	        identifier : 'thursday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     },
	     friday : {
	        identifier : 'friday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     },
	     saturday : {
	        identifier : 'saturday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     },
	     sunday : {
	        identifier : 'sunday',
	        rules : [ {
	           type : 'empty',
	           prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        } ]
	     }
      },
      onSuccess : function(event) {
         event.preventDefault();         
         settingformSubmission();
      }
   });
}

function settingformSubmission() {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES_SETTINGS_SAVE}}]];
		
	ajaxpost(url, $("#setting-form").serialize(), "div[id='settings-list']", true, function() {
	   initialize();
	});
}

function addSetting(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES_SETTINGS_CREATE}}]] + '/' + id;
	
    ajaxget(url, "div[id='modal']", function() {
    	initializeSettings();
    }, true);
}

function editSetting(id) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES_SETTINGS_UPDATE}}]] + '/' + id;
	
    ajaxget(url, "div[id='modal']", function() {
    	initializeSettings();
    }, true);
}

function deleteSetting(id){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SCHEDULES_SETTINGS_DELETE}}]] + '/' + id;
	
    $("#modal-confirmation")
    	.modal({closable: false,
    		onApprove: function() {
    			ajaxget(url, "div[id='settings-list']", function() {
    				initialize();
    			}, false);
    		},
    		onDeny: function() {
    		}
	}).modal('show');         
}
$(document).ready(function() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_CREATE}}]];

  if (id != 0) {
    url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  } else {
		    CKEDITOR.replace('description', {
		      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
		      toolbar : 'Basic',
		      uiColor : '#CDD1D3'
		    });
  }
   
  ajaxtab(true, url, true, function(settings) {
	    switch(settings.urlData.tab) {
	      case "userstories":
	        getuserstories();
	        break;
	      case "notes":
	        getNotes();
	        break;
	      case "tags":
	        gettags();
	        break;
	      case "history":
	        gethistory();
	        break;	        	 
	      case "info":
	      default:
	        getinfo();
	      break;
	    }
  });
   
  initialize();
});
        
function initialize() {
	  $('.ui.radio.checkbox').checkbox();
	  $('.ui.checkbox').checkbox();
	  $('.ui.toggle.checkbox').checkbox();
	  $('.ui.dropdown').dropdown();
	  $('select.dropdown').dropdown();	

	  formValidation();
}

function getinfo() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_AJAX}}]] + '/' + id;

  ajaxget(url, "div[data-tab='info']", function () {
    initialize();

    $('.ui.dropdown').dropdown({
      ignoreDiacritics: true,
      sortSelect: true,
      fullTextSearch:'exact'
    });
  });
};

function gettags() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_TAGS_AJAX}}]] + '/' + id;

  ajaxget(url, "div[data-tab='tags']", function () {
    initialize();
  });
}

function gethistory() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_HISTORY_AJAX}}]] + '/' + id;

  ajaxget(url, "div[data-tab='history']", function () {
    initialize();
  });
}

function getuserstories() {
  var id = $("input[type=hidden][name='id']").val();
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_USERSTORIES_AJAX}}]] + '/' + id;

  ajaxget(url, "div[data-tab='userstories']", function () {
    initialize();
  });
}

function getNotes() {
	  var id = $("input[type=hidden][name='id']").val();
	  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_NOTES_AJAX}}]] + '/' + id;
	   
	  ajaxget(url, "div[data-tab='notes']", function() {
	    initialize();
	  });
};

function createNote(id) {
	  var epicId = document.getElementById("epicId").value;
	  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_NOTES_NEW}}]] + '/' + epicId + "/" + id;
	
	  ajaxget(url, "div[id='modal']", function() {
	    CKEDITOR.replace('content', {customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]]});
	    
	    $('.ui.dropdown').dropdown();
	    $('select.dropdown').dropdown();
	    $('form').form({
		      on : 'blur',
		      inline : true,
		      fields : {
		    	    content : {
		    	      identifier : 'content',
			          rules : [ {
			            type : 'ckeditorvalidator',
			            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
			            value: "textarea[id='content']"
			          }]
			        }
		      },
		      onSuccess : function(event) {
		        event.preventDefault();
		        
		        var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_NOTES_SAVE}}]];  
		        ajaxpost(url, $("#epicNote-form").serialize(), "div[id='notes-list']", true, function() {
		          initialize();
		        });
		      }
	    });
	  }, true); 
};

function updateteam(epic,team) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS}}]] + '/' + epic + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_UPDATETEAM}]] + '/' + team;
	 
  ajaxget(url, "div[data-tab='info']", function() {
    initialize();
  }, false);
}

function addtag(epic) {
	  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_TAG_ADD}}]] + '/' + epic;
	
	  ajaxget(url, "div[id='modal']", function() {
	    $('.ui.checkbox').checkbox();
	    $('.ui.toggle.checkbox').checkbox();
	    $("#tags-form").form({
		      onSuccess : function(event) {
		        event.preventDefault();
		        
	        var urlpost = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_TAG_SAVE}}]];
	        ajaxpost(urlpost, $("#tags-form").serialize(), "div[id='tags-list']", true, function() {
	          initialize();
	        });
		      }
	    })
	  }, true);
}

function adduserstory(epic) {
	  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_STORY_CREATE}}]] + '/' + epic;
    
	  ajaxget(url, "div[id='modal']", function() {
	    $('.ui.dropdown').dropdown();
	    $('select.dropdown').dropdown();
	    
			    CKEDITOR.replace('description', 			{
			      customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
			      toolbar : 'Basic',
			      uiColor : '#CDD1D3'
			    });
			    
			    $("#userstories-form").form({
			      on : 'blur',
			      inline : true,
			      fields : {
			    	    name : {
				          identifier : 'name',
				          rules : [ {
				            type : 'empty',
				            prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
				          }]
			    	    },
			    	    team : {
				          identifier : 'team',
				          rules : [ {
				        	    type : 'integer[1..]',
				        	    prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
				          } ]
			    	    }
			      },
			      onSuccess : function(event) {
			        event.preventDefault();
			        
		        for (instance in CKEDITOR.instances) {
		          CKEDITOR.instances[instance].updateElement();
		        } 
		        
		        var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_STORY_SAVE}}]];  
		        ajaxpost(url, $("#userstories-form").serialize(), "div[id='userstories-list']", true, function() {
		          initialize();
		        });
		      }
			    });		
	  }, true);
}

function formValidation() {
  $('form').form({
    on : 'blur',
    inline : true,
    fields : {
    	  name : {
    	    identifier : 'name',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
    	  },
   	  	 team : {
   	  	   identifier : 'team.id',
   	  	   rules : [ {
   	  	     type : 'integer[1..]',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
   	  	   }]
   	  	 }
    }
  });
}

function updateState(id, status) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_STATUS_UPDATE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + status;
  
  ajaxget(url, "div[data-tab='info']", function() {
		    $('.ui.dropdown').dropdown();
	   }, false);
};

function storyformValidation() {
  $('#userstories-form').form({
    on : 'blur',
    inline : true,
    fields : {
      name : {
        identifier : 'name',
        rules : [{
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        }]
      }
    },
    onSuccess : function(event) {
      event.preventDefault();

      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_STORY_SAVE}}]];   
      ajaxpost(url, $("#userstories-form").serialize(), "", false, function() {
        initialize();
      });         
    }
  });
}


function editCampo(campo, header) {
	  var epic = $("input[type=hidden][name='id']").val();
	  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_INFO_FIELD}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + epic + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo  + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + header;

	  ajaxget(url, "div[id='modal']", function() {
	    formSubmissionedit(campo);
	    if (campo == 'description'){
	      CKEDITOR.replace('description', 	{
	        customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]],
					        toolbar : 'Basic',
					        uiColor : '#CDD1D3'
	      });
	    }	
	  }, true);
};

function formSubmissionedit(campo) {
	  $("#epicedit-form").form({	
	    on : 'blur',
	    inline : true,
	    fields : {
	      name : {
	        identifier : 'name',
	        rules : [ {
	          type : 'empty',
	          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	        }]
	      }
	    },
	    onSuccess : function(event) {
	      event.preventDefault();
	      
	      for (instance in CKEDITOR.instances) {
	    	    CKEDITOR.instances[instance].updateElement();
	    	  } 
	      
	      var id = $("input[type=hidden][name='id']").val();
	      var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_INFO_FIELD_UPDATE}}]]  + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + campo;
      
      ajaxpost(url, $("#epicedit-form").serialize(), "label[id='"+campo+"EP']", false, function() {
        initialize();
      }, returnurl);
	    }
	  });		
}

function filter() {
  var url  = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_EPICS_FILTER}}]]
      ajaxget(url, "div[id='modal']", function() {
    	  $('.ui.checkbox').checkbox();
    	  $('.ui.toggle.checkbox').checkbox();
    	  $('#filter-form').form({
    		  onSuccess : function(event) {
    			  event.preventDefault();               
    			  ajaxpost(url, $("#filter-form").serialize(), "div[id='list-epics']", true, function() {
    				  initialize();
    			  });
    		  }
    	  });
      }, true);
  }
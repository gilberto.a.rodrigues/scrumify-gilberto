package pt.com.scrumify.helpers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@AutoConfigureMockMvc
@DisplayName("Test of teams helper")
@SpringBootTest
public class TeamsHelperTest {
   @Autowired
   TeamsHelper helper;

   @DisplayName("Test of listOfResources method")
   @ParameterizedTest
   @UnitTest
   @ValueSource(ints = {1, 2})
   public void listOfResources(Integer id) throws Exception {
      List<Resource> members = helper.listOfResources(id);
      
      assertNotNull(members);
   }
}
package pt.com.scrumify.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;
import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Role;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.entities.ScrumifyUser;
import pt.com.scrumify.helpers.ConstantsHelper;

import java.util.ArrayList;
import java.util.List;

public class MockScrumifyUserWithSecurityContextFactory implements WithSecurityContextFactory<MockScrumifyUser> {
   @Autowired
   ResourceService resourcesService;
   
   @Override
   public SecurityContext createSecurityContext(MockScrumifyUser mock) {
      SecurityContext context = SecurityContextHolder.createEmptyContext();
      
      /*
       * Get resource
       */
      Resource resource = resourcesService.getByUsername(mock.username());

      /*
       * Get resource roles
       */
      List<GrantedAuthority> authorities = new ArrayList<>(0);
      for (Role role : resource.getProfile().getRoles()) {
         authorities.add(new SimpleGrantedAuthority(ConstantsHelper.ROLE_PREFIX + role.getName().toUpperCase()));
      }
      
      /*
       * User account status
       */
      boolean enabled = true;
      boolean accountNonExpired = true;
      boolean credentialsNonExpired = true;
      boolean accountNonLocked = true;
      ScrumifyUser user = new ScrumifyUser(resource.getUsername(), resource.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
      
      user.setResource(resource);
      user.setMenu(new ArrayList<Menu>());
      
      Authentication authentication = new UsernamePasswordAuthenticationToken(user, user.getPassword(), authorities);
      context.setAuthentication(authentication);
      
      return context;
   }
}
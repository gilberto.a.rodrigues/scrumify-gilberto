package pt.com.scrumify.configuration;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = MockScrumifyUserWithSecurityContextFactory.class)
public @interface MockScrumifyUser {
   String username() default "admin";
}
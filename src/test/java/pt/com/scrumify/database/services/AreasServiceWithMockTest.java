package pt.com.scrumify.database.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.database.entities.Area;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@AutoConfigureMockMvc
@DisplayName("Test of areas service")
@SpringBootTest
public class AreasServiceWithMockTest {   
   @MockBean
   AreaService service;

   @DisplayName("Test of save method")
   @Test
   @UnitTest
   public void save() throws Exception {
      Area area = new Area();
      area.setAbbreviation(RandomStringUtils.random(4, true, false));
      area.setName(RandomStringUtils.random(50, true, false));
      
      when(service.save(any(Area.class))).thenReturn(new Area());
      
      Area result = service.save(area);
      
      assertNotNull(result);
   }
}
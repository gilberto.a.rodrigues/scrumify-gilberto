package pt.com.scrumify.database.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureMockMvc
@DisplayName("Test of areas service")
@SpringBootTest
public class AreasServiceTest {
   private static int DEFAULT_AREA = 1;
   private static int DEFAULT_RESOURCE = 2;
   
   @Autowired
   AreaService service;

   @DisplayName("Test of getOne method")
   @Test
   @UnitTest
   public void getOne() throws Exception {
      Area area = service.getOne(DEFAULT_AREA);
      
      assertNotNull(area);
      assertAll("area",
            () -> assertEquals(1, area.getId()),
            () -> assertEquals("AGCI", area.getAbbreviation()),
            () -> assertEquals("Área de Gestão de Contribuintes e Inspeção", area.getName())
            );
   }

   @DisplayName("Test of listAll method")
   @Test
   @UnitTest
   public void getAll() throws Exception {
      List<Area> areas = service.listAll();
      
      assertNotNull(areas);
      assertEquals(3, areas.size());
   }

   @DisplayName("Test of getByResource method")
   @Test
   @UnitTest
   public void getByResource() throws Exception {
      List<Area> areas = service.getByResource(new Resource(DEFAULT_RESOURCE));
      
      assertNotNull(areas);
      assertEquals(1, areas.size());
   }
}
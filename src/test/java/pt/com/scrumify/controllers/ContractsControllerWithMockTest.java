package pt.com.scrumify.controllers;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@DisplayName("Test of contracts controller")
@SpringBootTest
public class ContractsControllerWithMockTest {
   @Autowired
   private MockMvc mvc;
   @MockBean
   ContractService mock;

   @DisplayName("Test of areas save mapping (post) by not allowed user (projectlead1)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")  
   public void saveByNotAllowedUser() throws Exception {
      String content = "id=0&" +
                       "abbreviation=" + RandomStringUtils.random(4, true, false) + "&" +
                       "year=" + DatesHelper.year() + "&" +
                       "name=" + RandomStringUtils.random(50, true, false);
      
      when(mock.save(any(Contract.class))).thenReturn(new Contract());
            
      mvc.perform(post(ConstantsHelper.MAPPING_CONTRACTS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isForbidden())
         .andReturn()
         ;
   }
   
   @DisplayName("Test of areas save mapping (post) by allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void saveByAllowedUser() throws Exception {
      String content = "id=0&" +
                       "abbreviation=" + RandomStringUtils.random(4, true, false) + "&" +
                       "year=" + DatesHelper.year() + "&" +
                       "name=" + RandomStringUtils.random(50, true, false);
      
      when(mock.save(any(Contract.class))).thenReturn(new Contract());
            
      mvc.perform(post(ConstantsHelper.MAPPING_CONTRACTS_SAVE)
                  .content(content)
                  .with(csrf())
                  .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                  .accept(MediaType.APPLICATION_FORM_URLENCODED))
         .andExpect(status().isFound())
         .andExpect(redirectedUrl(ConstantsHelper.MAPPING_CONTRACTS))
         .andReturn()
         ;
   }
}
package pt.com.scrumify.controllers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import pt.com.scrumify.annotations.SmokeTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.helpers.ConstantsHelper;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@DisplayName("Test of absences controller")
@SpringBootTest
public class AbsencesControllerTest {
   @Autowired
   private MockMvc mvc;
   @Autowired
   AbsenceService service;

   @DisplayName("Test of absences list mapping (get) by not allowed user (anonymous)")
   @Test
   @SmokeTest
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_ABSENCES))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of absences list mapping (get) by allowed user (projectdirector)")
   @Test
   @SmokeTest
   @MockScrumifyUser(username = "developer3")
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      
      mvc.perform(get(ConstantsHelper.MAPPING_ABSENCES))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_ABSENCES_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, hasSize(EXPECTED_SIZE)))
         ;
   }
}
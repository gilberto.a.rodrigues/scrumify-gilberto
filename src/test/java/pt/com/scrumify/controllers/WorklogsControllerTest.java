package pt.com.scrumify.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import pt.com.scrumify.annotations.SmokeTest;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.WorkItemWorkLogView;
import pt.com.scrumify.helpers.ConstantsHelper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DisplayName("Test of worklogs controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WorklogsControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_WORKLOG + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @SmokeTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_WORKLOG + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @SmokeTest
   @Order(3)
   @MockScrumifyUser(username = "developer3")  
   public void createByAssignedUser() throws Exception {
      int workItem = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_NEW + ConstantsHelper.MAPPING_SLASH + workItem))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_WORKITEMS_WORKLOG_NEW))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG, isA(WorkItemWorkLogView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG, hasProperty("id", is(0))))
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer3")
   public void updtateNotAllowedDueToTimesheetSubmitted() throws Exception {
      int worklog = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_UPDATE + ConstantsHelper.MAPPING_SLASH + worklog))
         .andExpect(status().isForbidden())
         ;
   }
   
}
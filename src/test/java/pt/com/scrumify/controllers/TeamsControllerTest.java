package pt.com.scrumify.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.TeamView;
import pt.com.scrumify.helpers.ConstantsHelper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DisplayName("Test of teams controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TeamsControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_TEAMS))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_TEAMS_CREATE))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(3)
   @MockScrumifyUser(username = "projectdirector")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      
      mvc.perform(get(ConstantsHelper.MAPPING_TEAMS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_TEAMS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, hasItem(allOf(hasProperty("id", is(1)),
                                                                                          hasProperty("name", is("NIGC")),
                                                                                          hasProperty("active", is(true))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, hasItem(allOf(hasProperty("id", is(2)),
                                                                                          hasProperty("name", is("NSAI")),
                                                                                          hasProperty("active", is(true))))))
         ;
   }
   
   @Test
   @UnitTest
   @Order(4)
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_TEAMS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_TEAMS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, isA(TeamView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, hasProperty("name", is(emptyOrNullString()))))
         ;
   }
}